package ISAssignment;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * Simulated Solar Panel Agent. The solar panel generates electricty based on
 * the time of day (and doesn't work at all during the night). It sends
 * electricity to a battery.
 *
 * The battery is sent power generation with an inform message, where the
 * content is the power output in Joules (eg. 36530), with the converstaion id
 * "application-generation".
 */
public class SolarPanelAgent extends Agent {

	/**
	 * Calculate panel efficiency based on current time. The intention here is to
	 * allow us to later have some timekeeper, and get the time from that. In the
	 * future this should be extended to use historical data to show seasonal
	 * variations. Currently efficiency is based solely on the hour.
	 *
	 * @param lt
	 *            The current time.
	 * @return efficiency as a decimal, eg. 0.2 = 20%.
	 */
	static double efficiency_at_time(LocalDateTime lt) {
		int hour = lt.getHour();
		double efficiency;
		// Panels begin producing power at 6am, and stop at 6pm
		// Power output follows an elliptical curve, with the peak at midday.
		efficiency = Math.sqrt(36 - Math.pow(hour - 12, 2)) / 6;
		// Our efficiency function is undefined during certain parts of the day. Return
		// 0 for those parts.
		if (Double.isNaN(efficiency)) {
			efficiency = 0;
		}
		return efficiency;
	}

	private static List<Float> solarData = new ArrayList<Float>();
	private Integer iteration = 0;
	private String batteryAgentName;
	private long maximumOutput;

	@Override
	protected void setup() {
		// Two arguments: name of connected battery & maximum power output in Watts.
		Object[] args = getArguments();
		batteryAgentName = args[0].toString();
		maximumOutput = Long.parseLong(args[1].toString());

		addBehaviour(new SolarPanelTickerBehaviour(this, 10000));
	}

	private class SolarPanelTickerBehaviour extends TickerBehaviour {

		private final SolarPanelAgent spa;

		public SolarPanelTickerBehaviour(SolarPanelAgent a, long period) {
			super(a, period);
			this.spa = a;
		}

		@Override
		protected void onTick() {
			long powerGenerated = Math.round(spa.maximumOutput * efficiency_at_time(LocalDateTime.now()));
            float temp = (float)0;
			ACLMessage msg = new ACLMessage(ACLMessage.CFP);
			msg.addReceiver(new AID(spa.batteryAgentName, AID.ISLOCALNAME));
		    if(iteration < solarData.size()) {
				temp = solarData.get(iteration);
				iteration +=1;
				msg.setContent(String.valueOf(temp)); // how much energy need
				System.out.println(getLocalName() + " sent power Generation" + temp +" to " + spa.batteryAgentName);
				Gui.setSolarPanelText(temp);
			}else {
				msg.setContent(String.valueOf(powerGenerated)); // how much energy need
				 System.out.println(getLocalName() + " sent power Generation" + powerGenerated +" to " + spa.batteryAgentName);
				 Gui.setSolarPanelText(powerGenerated);
			}
            
			msg.setConversationId("application-generation");
			send(msg);
		

			// update solar charge text in Gui
			Gui.setSolarPanelText(powerGenerated);
		}

	}

	public static void setCurrentCharge(float parseFloat) {
		solarData.add(parseFloat);
		System.out.println("Current charge to Solar Panel in [kWs]: " + Float.toString(parseFloat));
	}
}
