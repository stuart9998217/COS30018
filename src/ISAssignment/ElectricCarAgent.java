package ISAssignment;

import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

/**
 * ElectricCar agent will answer for home agent quest and cosuming energy from
 * battery This agent using RequestProcessingServer to sent response using
 * ChargeQuest to inform home agent and get a permission Using ChargeProcess to
 * consuming energy
 * 
 * @author qiyuan zhu (100858830)
 **/
public class ElectricCarAgent extends Agent {
	private String serviceName = "";
	private List<AID> subscribers = new ArrayList<AID>();
	private static List<Float> carData = new ArrayList<Float>();
	private int iteration = 0;
	private int carbattery = 1000;
	private int carbattery_now = 0;
	private boolean permission = false;

	protected void setup() {
		Object[] args = getArguments();
		serviceName = args[0].toString();

		// Description of service to be registered
		ServiceDescription sd = new ServiceDescription();
		sd.setType("application");
		sd.setName(serviceName);
		register(sd);

		// Add behaviour that receives messages and reply
		addBehaviour(new RequestProcessingServer());

		// this ticking is aiming to update if the battery has enough energy for at
		// least a short period of usage
		addBehaviour(new TickerBehaviour(this, 80000) {
			protected void onTick() {
				if (carbattery > carbattery_now && permission == false) {
					addBehaviour(new ChargeQuest());
				}
			}
		});

		// this ticking is charge the car battery
		addBehaviour(new TickerBehaviour(this, 10000) {
			protected void onTick() {
				if (carbattery > carbattery_now && permission == true) {
					addBehaviour(new ChargeProcess());
				}

			}
		});
	}

	// Method to register the service
	void register(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd); // An agent can register one or more service

		// Register the agent and its services
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	private class RequestProcessingServer extends CyclicBehaviour {

		public void action() {
			ACLMessage msg = receive();
			if (msg != null) {
				// Check if receiving a subscription message
				if (msg.getConversationId().equals("customer-subscription")) {
					// We know home agent is here
					System.out.println("There is a home agent contacted us.");
					subscribers.add(msg.getSender());
				}

				if (msg.getConversationId().equals("application-car-charge")) {
					// know the permission from home to charge
					System.out.println("home agent sends permission: " + msg.getContent());
					String temp = msg.getContent().toString();
					if (temp.equals("yes")) {
						permission = true;
					} else {
						permission = false;
					}
				}

			} else {
				block();
			}
		}
	}

	// send inform to home, asking for charge permission
	private class ChargeQuest extends OneShotBehaviour {

		@Override
		public void action() {
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(subscribers.get(0));
			msg.setContent(String.valueOf(carbattery - carbattery_now));
			msg.setConversationId("application-car-charge");
			send(msg);
			System.out.println(getLocalName() + " sent charge quest to home");

			// TODO Auto-generated method stub

		}

	}

	// this process will try to ask battery for energy every 1 second when heating
	// is 'open'
	// if battery say 'no' which means don't have enough power, it will turn
	// 'close(off)'
	private class ChargeProcess extends Behaviour {

		public void action() {
			// ask battery give energy
			ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);

			cfpMsg.addReceiver(subscribers.get(0));
			if(iteration < carData.size()) {
				float temp = carData.get(iteration);
				iteration +=1;
				cfpMsg.setContent(String.valueOf(temp)); // how much energy need
				System.out.println("car charge is working, using "+ temp +"unit energy");
			}else {
				cfpMsg.setContent("20"); // how much energy need
				System.out.println("car charge is working, using 20 unit energy");
			}
			cfpMsg.setConversationId("application-require-energy");
			cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
			myAgent.send(cfpMsg);
			// Prepare the message template to get proposals
			System.out.println(carbattery - carbattery_now + " unit power still needed");

		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return true;
		}

	}

	// Method to send terminated notification to subscribers and de-register the
	// service (on take down)
	protected void takeDown() {
		try {
			// Send notification
			ACLMessage noti = new ACLMessage(ACLMessage.INFORM);
			noti.setConversationId("retailer-terminated");
			noti.setContent("went bankrupt");
			for (AID receiver : subscribers) {
				noti.addReceiver(receiver);
			}
			send(noti);

			// De-register from DF agent
			DFService.deregister(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(getAID().getName() + " terminated");
	}

	public static void setCurrentUsage(float parseFloat) {
		carData.add(parseFloat);
		System.out.println("Current Car charger [kWs] usage: " + Float.toString(parseFloat));
	}
}
