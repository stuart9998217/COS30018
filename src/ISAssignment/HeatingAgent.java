package ISAssignment;

import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

/**
 * 
 * This agent using RequestProcessingServer to sent response Using HeaingProcess
 * to consuming energy
 * 
 * @author qiyuan zhu (100858830)
 **/

public class HeatingAgent extends Agent {
	private String serviceName = "";
	private List<AID> subscribers = new ArrayList<AID>();
	private static List<Float> heatingData = new ArrayList<Float>();
	private Integer iteration = 0;

	protected void setup() {
		Object[] args = getArguments();
		serviceName = args[0].toString();

		// Description of service to be registered
		ServiceDescription sd = new ServiceDescription();
		sd.setType("application");
		sd.setName(serviceName);
		register(sd);

		// Add behaviour that receives messages and reply
		addBehaviour(new RequestProcessingServer());

		// this ticking is try to stimulate the real time energy consuming
		// in this fast ticking it will try to consume energy every 0.1 second by x
		// units if 'open' is true
		addBehaviour(new TickerBehaviour(this, 10000) {
			protected void onTick() {
				addBehaviour(new HeatingProcess());
			}
		});

	}

	// Method to register the service
	void register(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd); // An agent can register one or more service

		// Register the agent and its services
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	private class RequestProcessingServer extends Behaviour {

		public void action() {
			ACLMessage msg = receive();
			if (msg != null) {
				// Check if receiving a subscription message
				if (msg.getConversationId().equals("customer-subscription")) {
					// We know home agent is here
					System.out.println("There is a home agent contacted us.");
					subscribers.add(msg.getSender());
				}

				/*
				 * if(msg.getConversationId().equals("close-heating")) {
				 * System.out.println(" get order from home to "+ msg.getContent().toString());
				 * // should try to close heating // reset all condition open = false;
				 * System.out.println("now heating close"); }
				 * 
				 * if(msg.getConversationId().equals("open-heating")) {
				 * System.out.println(" get order from home to "+ msg.getContent().toString());
				 * ACLMessage reply = msg.createReply();
				 * reply.setPerformative(ACLMessage.PROPOSE); if(enoughenergy) {
				 * reply.setContent("yes"); myAgent.send(reply); open = true;
				 * System.out.println("open heating now"); }else { reply.setContent("no");
				 * myAgent.send(reply); open = false;
				 * System.out.println("can not open heating now"); } }
				 * if(msg.getConversationId().equals("application-heating")) {
				 * 
				 * System.out.println("Response received"); // Response received
				 * System.out.println(msg.getContent());
				 * System.out.println(msg.getSender().getLocalName() + " has energy: " +
				 * msg.getContent()); int energy = Integer.parseInt(msg.getContent()); if(energy
				 * > 100){ enoughenergy = true;
				 * 
				 * // we have enough enery to start heating }else{ // we cannot open heating
				 * because the battery don't have enough energy enoughenergy = false;
				 * 
				 * } }
				 */

			} else {
				block();
			}
		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return false;
		}

	}

	/*
	 * // asking battery if there is enough energy private class UpdatingProcess
	 * extends Behaviour { private MessageTemplate mt; private String state = "";
	 * 
	 * public void action() {
	 * 
	 * ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);
	 * 
	 * 
	 * //ask battery if there is enough energy to start
	 * 
	 * cfpMsg.addReceiver(new AID("Battery", AID.ISLOCALNAME));
	 * cfpMsg.setConversationId("application-heating"); cfpMsg.setReplyWith("cfp" +
	 * System.currentTimeMillis()); // Unique value
	 * 
	 * myAgent.send(cfpMsg); // Prepare the message template to get proposals state
	 * = "done"; System.out.println("send quest to ask battery");
	 * 
	 * };
	 * 
	 * @Override public boolean done() { // TODO Auto-generated method stub return
	 * state == "done"; } }
	 */

	// this process will try to ask battery for energy every 1 second when heating
	// is 'open'
	private class HeatingProcess extends Behaviour {

		// ask tell home the usage
		ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);

		@Override
		public void action() {
			cfpMsg.addReceiver(new AID("home", AID.ISLOCALNAME));
			if(iteration < heatingData.size()) {
				float temp = heatingData.get(iteration);
				iteration +=1;
				cfpMsg.setContent(String.valueOf(temp)); // how much energy need
				System.out.println("heating is working, using "+ temp +" unit energy");
			}else {
				cfpMsg.setContent("15"); // how much energy need
				System.out.println("heating is working, using 15 unit energy");
			}
			cfpMsg.setConversationId("application-require-energy");
			cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
			myAgent.send(cfpMsg);



		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return true;
		}

	}

	// Method to send terminated notification to subscribers and de-register the
	// service (on take down)
	protected void takeDown() {
		try {
			// Send notification
			ACLMessage noti = new ACLMessage(ACLMessage.INFORM);
			noti.setConversationId("retailer-terminated");
			noti.setContent("went bankrupt");
			for (AID receiver : subscribers) {
				noti.addReceiver(receiver);
			}
			send(noti);

			// De-register from DF agent
			DFService.deregister(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(getAID().getName() + " terminated");
	}

	public boolean done() {
		// TODO Auto-generated method stub
		return false;
	}

	public static void setCurrentUsage(float parseFloat) {
		heatingData.add(parseFloat);
		System.out.println("Current Heating [kWs] usage: " + Float.toString(parseFloat));
	}
}
