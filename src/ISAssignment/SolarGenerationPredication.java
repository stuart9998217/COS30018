package ISAssignment;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;  

public class SolarGenerationPredication extends TimerTask{
	private Integer i = 0;
	private boolean read = false;
	private String[] x = null ;
	private static float predication = (float) 0;
	
	public static float getPredication() {
		return predication;
	}

	@Override
	public void run() {
		String fileName = "predicationResult.txt";
		//System.out.println("now timer open");
	    String line = null;
	    try {
	        // FileReader reads text files in the default encoding.
	        FileReader fileReader = 
	            new FileReader(fileName);

	        // Always wrap FileReader in BufferedReader.
	        BufferedReader bufferedReader = 
	            new BufferedReader(fileReader);

	        while((line = bufferedReader.readLine()) != null && read == false) {
	        	int endpoint = line.length();
	        	line = line.substring(1, endpoint-2);
	        	line = line.replace("],[", ",");
	        	x = line.split(",");      
	        	read = true;
	        }   
	        float xx =0;

	        for(int ii = 0; ii < 24; ii++) {
	        	if(i+ii < x.length) {
	        		xx += Float.valueOf(x[i+ii]);
	            	//System.out.println(x[i+ii]);
	        	}else {
	        		//System.out.println("complete the list");
	        	}
	         }
	        i += 24;
	        System.out.println((xx-24)) +" is the predication of next days' power generation");
	        predication = xx-24;
	        // Always close files.
	        bufferedReader.close();    
	       
	        
	    }
	    catch(FileNotFoundException ex) {
	        System.out.println(
	            "Unable to open file '" + 
	            fileName + "'");                
	    }
	    catch(IOException ex) {
	        System.out.println(
	            "Error reading file '" 
	            + fileName + "'");                  
	        // Or we could just do this: 
	        // ex.printStackTrace();
	    }

	}  
	} 