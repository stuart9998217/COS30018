
/*
	 InDemandRetailerAgent has very low rate, but you go over your usage you will be charged a very high penalty rate.
*/

package ISAssignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class LimitedRetailerAgent extends Agent {
	Timer timer;
	// this retailer send information for these customer
	private List<AID> subscribers = new ArrayList<AID>();
	// this retailer agent has such name
	private String serviceName = "";

	// this retailer has a low initial rate of 9.433 cents/ kWh
	private float sellingPrice = (float) 9.433;

	private float KWsOfferToBattery = 0;

	private boolean ContractOn = false;

	public void TimerInWorking() {
		timer = new Timer();
		timer.schedule(new TimerTaskInfromOpen(), 14500, 24000);
		timer.schedule(new TimerTaskInfromClose(), 16500, 24000);
	}

	public class TimerTaskInfromOpen extends TimerTask {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(new AID("Home", AID.ISLOCALNAME));
			msg.setContent("open");
			msg.setConversationId("limited-charge");
			send(msg);
			System.out.println(getLocalName() + " is open");
		}

	}

	public class TimerTaskInfromClose extends TimerTask {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.addReceiver(new AID("Home", AID.ISLOCALNAME));
			msg.setContent("close");
			msg.setConversationId("limited-charge");
			send(msg);
			System.out.println(getLocalName() + " is close");
		}

	}

	protected void setup() {
		// Only 1 argument is the local name of the home agent
		Object[] args = getArguments();
		serviceName = args[0].toString();
		// Description of service to be registered
		ServiceDescription sd = new ServiceDescription();

		sd.setType("energy-selling");
		sd.setName(serviceName);
		register(sd);

		System.out.println("Agent " + getLocalName() + " waiting for requests...");
		addBehaviour(new RequestProcessingServer());
		TimerInWorking();

		// supply energy to battery agent
		addBehaviour(new TickerBehaviour(this, 10000) {
			protected void onTick() {
				if (ContractOn) {
					ACLMessage msg = new ACLMessage(ACLMessage.CFP);
					msg.addReceiver(new AID("Battery", AID.ISLOCALNAME));
					msg.setContent(String.valueOf(KWsOfferToBattery));
					KWsOfferToBattery = 0;
					ContractOn = false;
					msg.setConversationId("application-generation");
					send(msg);
					System.out.println(getLocalName() + " sent power Generation to Battery");
				} else {
					System.out.println("Currently has no contract");
				}
			}
		});

		/*
		 * MessageTemplate template = MessageTemplate.and(
		 * MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
		 * MessageTemplate.MatchPerformative(ACLMessage.REQUEST) );
		 * 
		 * 
		 * addBehaviour(new AchieveREResponder(this, template) { protected ACLMessage
		 * prepareResponse(ACLMessage request) throws NotUnderstoodException,
		 * RefuseException { System.out.println("Agent " + getLocalName() +
		 * ": REQUEST received from " + request.getSender().getName() + ". Action is " +
		 * request.getContent()); if (checkAction(request)) { // We agree to perform the
		 * action. Note that in the FIPA-Request // protocol the AGREE message is
		 * optional. Return null if you // don't want to send it.
		 * System.out.println("Agent " + getLocalName() + ": Agree"); ACLMessage agree =
		 * request.createReply(); agree.setPerformative(ACLMessage.AGREE); return agree;
		 * } else { // We refuse to perform the action System.out.println("Agent " +
		 * getLocalName() + ": Refuse"); throw new RefuseException("check-failed"); } }
		 * 
		 * protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage
		 * response) throws FailureException { if (performAction(request)) {
		 * System.out.println("Agent " + getLocalName() +
		 * ": Action successfully performed"); ACLMessage inform =
		 * request.createReply(); inform.setPerformative(ACLMessage.INFORM); return
		 * inform; } else { System.out.println("Agent " + getLocalName() +
		 * ": Action failed"); throw new FailureException("unexpected-error"); } } } );
		 */

		/*
		 * addBehaviour(new CyclicBehaviour() { public void action() { ACLMessage msg =
		 * receive();
		 * 
		 * if (msg != null) { if (msg.getConversationId().equals("buy-electricity")) {
		 * System.out.println(msg.getSender().getLocalName() +
		 * " has requested the buying price of " + msg.getContent() + " kWs");
		 * kWsRequested = Float.parseInt(msg.getContent());
		 * 
		 * // the price that the home will pay if they don't exceed the amount of
		 * requested electricity float price = supplyCharge + kWsRequested *
		 * lowSellingPrice;
		 * 
		 * reply.addReceiver(new AID(args[0].toString(), AID.ISLOCALNAME));
		 * reply.setContent(price);
		 * reply.setConversationId("limitedRetailer-sellingPrice"); send(reply); } else
		 * if (msg.getConversationId().equals("buy-startTime")) {
		 * System.out.println(msg.getSender().getLocalName() +
		 * " has requested when it will start selling electricity");
		 * 
		 * // the penalty rate, which is 62.666 per kW exceeded reply.addReceiver(new
		 * AID(args[0].toString(), AID.ISLOCALNAME)); reply.setContent(startTime);
		 * reply.setConversationId("limitedRetailer-penaltyRate"); send(reply); } else
		 * if (msg.getConversationId().equals("buy-stopTime")) {
		 * System.out.println(msg.getSender().getLocalName() +
		 * " has requested when it will stop selling electricity");
		 * 
		 * // the penalty rate, which is 62.666 per kW exceeded reply.addReceiver(new
		 * AID(args[0].toString(), AID.ISLOCALNAME)); reply.setContent(stopTime);
		 * reply.setConversationId("limitedRetailer-penaltyRate"); send(reply); } else
		 * if (msg.getConversationId().equals("sell-electricity")) {
		 * System.out.println(msg.getSender().getLocalName() +
		 * " has requested the selling price of " + msg.getContent() + " kWs");
		 * kWsOffered = Float.parseInt(msg.getContent());
		 * 
		 * ACLMessage reply = new ACLMessage(ACLMessage.INFORM); reply.addReceiver(new
		 * AID(args[0].toString(), AID.ISLOCALNAME)); reply.setContent(sellingPrice);
		 * reply.setConversationId("limitedRetailer-buyingPrice"); send(reply); } else {
		 * block(); } } else { block(); } } });
		 */
	}

	private class RequestProcessingServer extends CyclicBehaviour {
		
		private float amount;
		public void action() {
			ACLMessage msg = receive();
			
			if (msg != null) {
				// Check if receiving a subscription message
				if (msg.getPerformative() == ACLMessage.INFORM
						&& msg.getConversationId().equals("customer-subscription")) {
					// We know home agent is here
					System.out.println("There is a home agent contacted us.");
					subscribers.add(msg.getSender());
				}

				// limited agent doesn't provide contract, it will only answer for amount
				// of buying in a limited time
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-buying")) {
					System.out.println("This agent doesn't provide energy in no-working time");
					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(0));
					reply.setPerformative(ACLMessage.REFUSE);

					send(reply);

				}
				if (msg.getPerformative() == ACLMessage.CFP
						&& msg.getConversationId().equals("energy-buying-small-amount")) {
					System.out.println("This agent doesn't provide energy in no-working time");
					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(0));
					reply.setPerformative(ACLMessage.REFUSE);

					send(reply);

				}

				// handle limited time window, tell the price per unit
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-price-limited")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the buying price");
					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(sellingPrice));
					reply.setPerformative(ACLMessage.PROPOSE);

					send(reply);

				}
				// Send back the price if be asked
				if (msg.getPerformative() == ACLMessage.CFP
						&& msg.getConversationId().equals("energy-buying-limited")) {
					amount = Float.parseFloat(msg.getContent());
					System.out.println(
							msg.getSender().getLocalName() + " has requested the buying " + amount + " units of power");
					KWsOfferToBattery = amount;
					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(sellingPrice * amount));
					reply.setPerformative(ACLMessage.PROPOSE);
					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-confirm-limited")) {
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					confirm.setContent("This offer apply in limited retailer is done");
					myAgent.send(confirm);
					ContractOn = true;
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");
					
					Gui.updateScrollPaneText(
							msg.getSender().getLocalName() + " has accepted \n" + getLocalName() + 
							"'s contract offer to buy " + amount + " kWs, \nat "
							+ String.valueOf(sellingPrice) + " a kW, for a total cost of $"
							+ String.valueOf(sellingPrice * amount) + "\n"
					);
				}

				if (msg.getPerformative() == ACLMessage.REFUSE && msg.getConversationId().equals("refuse")) {
					System.out.println("Agent " + getLocalName() + ": Action failed");
					KWsOfferToBattery = 0;
				}

			} else {
				block();
			}
		}
	}

	// Method to register the service
	void register(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd); // An agent can register one or more services
		System.out.println("registered");

		// Register the agent and its services
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	// Method to de register the service (on take down)
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (Exception e) {
		}
	}

	/*
	 * private boolean checkAction(ACLMessage request) { return
	 * (request.getSender().getName() == "home"); }
	 * 
	 * private boolean performAction(ACLMessage request) { if (msg != null) { if
	 * (msg.getConversationId().equals("buy-electricity")) {
	 * System.out.println(msg.getSender().getLocalName() +
	 * " has requested the buying price of " + msg.getContent() + " kWs");
	 * kWsRequested = Float.parseInt(msg.getContent());
	 * 
	 * // the price that the home will pay if they don't exceed the amount of
	 * requested electricity float price = supplyCharge + kWsRequested *
	 * lowSellingPrice;
	 * 
	 * reply.addReceiver(new AID(args[0].toString(), AID.ISLOCALNAME));
	 * reply.setContent(price);
	 * reply.setConversationId("limitedRetailer-sellingPrice"); send(reply); } else
	 * if (msg.getConversationId().equals("buy-startTime")) {
	 * System.out.println(msg.getSender().getLocalName() +
	 * " has requested when it will start selling electricity");
	 * 
	 * // the penalty rate, which is 62.666 per kW exceeded reply.addReceiver(new
	 * AID(args[0].toString(), AID.ISLOCALNAME)); reply.setContent(startTime);
	 * reply.setConversationId("limitedRetailer-penaltyRate"); send(reply); } else
	 * if (msg.getConversationId().equals("buy-stopTime")) {
	 * System.out.println(msg.getSender().getLocalName() +
	 * " has requested when it will stop selling electricity");
	 * 
	 * // the penalty rate, which is 62.666 per kW exceeded reply.addReceiver(new
	 * AID(args[0].toString(), AID.ISLOCALNAME)); reply.setContent(stopTime);
	 * reply.setConversationId("limitedRetailer-penaltyRate"); send(reply); } else
	 * if (msg.getConversationId().equals("sell-electricity")) {
	 * System.out.println(msg.getSender().getLocalName() +
	 * " has requested the selling price of " + msg.getContent() + " kWs");
	 * kWsOffered = Float.parseInt(msg.getContent());
	 * 
	 * ACLMessage reply = new ACLMessage(ACLMessage.INFORM); reply.addReceiver(new
	 * AID(args[0].toString(), AID.ISLOCALNAME)); reply.setContent(sellingPrice);
	 * reply.setConversationId("limitedRetailer-buyingPrice"); send(reply); } else {
	 * block(); } } else { block(); } }
	 */
}