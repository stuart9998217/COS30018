package ISAssignment;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;

/**
 * Battery agent will answer any kind of energy-knowing/consuming quest
 * also regularly provide home agent its storage
 * if the storage is low, it will sent warning with storage instead
 * @author qiyuan zhu (100858830)
 **/
public class BatteryAgent extends Agent {
	private static float powerStoragelimit = 5000; // this will be used in next move
	public static float getPowerStorageLimit() {
		return powerStoragelimit;
	}
	public static void setPowerStorageLimit(float powerStoragelimit) {
		BatteryAgent.powerStoragelimit = powerStoragelimit;
	}
	public static float getPowerStored() {
		return powerStored;
	}
	public static void setPowerStored(float powerStored) {
		BatteryAgent.powerStored = powerStored;
	}
	private static float powerStored = 1;
	private float MaxOutput = (float)7.8; // the max energy output battery can provide
	private String agent;
	protected void setup () {
		// Only 1 argument is the local name of the home agent
		Object[] args = getArguments();
		agent = (String) args[0];
		powerStoragelimit = Integer.parseInt((String)args[1]);
		
		
		addBehaviour(new TickerBehaviour(this, 10000) {
			protected void onTick() {
				if(powerStored<2) {
					ACLMessage msg = new ACLMessage(ACLMessage.CFP);
					msg.addReceiver(new AID("Home", AID.ISLOCALNAME));
					msg.setContent(String.valueOf(powerStored));
					msg.setConversationId("application-battery-warning");
					send(msg);					
					System.out.println(getLocalName() + " sent warning to " + agent+" only have "+powerStored+" unit power");
					
				}else if(powerStored > 50){
					ACLMessage msg = new ACLMessage(ACLMessage.CFP);
					msg.addReceiver(new AID("Home", AID.ISLOCALNAME));
					msg.setContent(String.valueOf(powerStored));
					msg.setConversationId("application-battery-warning-full");
					send(msg);		
					System.out.println(getLocalName() + " power Storage is almost full." +" it has "+powerStored+" unit power");	
				
					
				}else {
					ACLMessage msg = new ACLMessage(ACLMessage.CFP);
					msg.addReceiver(new AID("Home", AID.ISLOCALNAME));
					msg.setContent(String.valueOf(powerStored));
					msg.setConversationId("application-battery");
					send(msg);		
					System.out.println(getLocalName() + " sent power Storage " + powerStored + " to " + args[0].toString());	
				}
				
			}
		});
		
		
		// inform home with energy output
		addBehaviour(new CyclicBehaviour() {
			public void action() {
				ACLMessage msg = receive();
				if (msg != null) {
				if (msg.getPerformative() == ACLMessage.CFP) {

					if (msg.getConversationId().equals("home-require-energy")) {
						
						System.out.println(msg.getSender().getLocalName() + " require " + msg.getContent() + " units of power");
						float i = Float.parseFloat(msg.getContent());
						float supply = 0;
						if(MaxOutput>i && powerStored > i  ) {
							powerStored -=i;
							supply = 0;
							System.out.println("condition1");
						}else if(MaxOutput>i && powerStored < i){
							
							supply =  i - powerStored ;
							powerStored = 0;
							System.out.println("condition2");
						}else if(MaxOutput < i && powerStored > i) {
							
							powerStored -= MaxOutput;
							supply = i - MaxOutput;
							System.out.println("condition3");
						}else {
							if(powerStored > MaxOutput ) {
								powerStored -= MaxOutput;
								supply = i - MaxOutput;
								System.out.println("condition4");
							}else {
							powerStored = 0;
							supply = i - powerStored;
							System.out.println("condition5");
							}
						}
						
							ACLMessage reply = msg.createReply();
							reply.setPerformative(ACLMessage.PROPOSE);
							reply.setContent(String.valueOf(supply));
							myAgent.send(reply);
							System.out.println("send back for ack energy needed with:" + supply);
							
						}
					
					if (msg.getConversationId().equals("application-generation")) {
						
						System.out.println( "Get energy from "+ msg.getSender().getLocalName() +" with " + msg.getContent() + " units of power");
						float temp = Float.parseFloat(msg.getContent());
						if((powerStored + temp)> powerStoragelimit) {
							powerStored = powerStoragelimit;
						}else {
							powerStored += Float.parseFloat(msg.getContent());
						}
					}
				}
			}else {	
					block();
				}		
				
			}
		});
	
	
}
}
