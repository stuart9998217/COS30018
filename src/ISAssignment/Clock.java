// @author Timothy Quill 100997474

package ISAssignment;

public class Clock {
	private static int hours;
	private static int days;
	private static int months;
	private boolean startClock = false;

	public Clock() {
		hours = 0;
		days = 1;
		months = 1;
	}

	public void update() {
		if (startClock) {
			hours++;
		}

		if (hours >= 24) {
			hours = 0;
			days++;

			if (days > 28 && months == 2) {
				days = 1;
			}
			if (days > 30 && (months == 4 || months == 6 || months == 9 || months == 11)) {
				days = 1;
			}
			if (days > 31) {
				days = 1;
			}
		}
	}

	public void resetClock() {
		hours = 0;
		days = 1;
		months = 1;
	}

	public String getTimeDate() {
		// Time/Date format: ss:mm:hh dd/mm/yyyy)
		return "01:01:" + addZeroToSingleDigit(hours) + " " + addZeroToSingleDigit(days) + "/"
				+ addZeroToSingleDigit(months) + "/2018";
	}

	public String addZeroToSingleDigit(int digit) {
		if (digit < 10) {
			return "0" + digit;
		} else {
			return Integer.toString(digit);
		}
	}

	public void startClock() {
		startClock = true;
	}

	public void stopClock() {
		startClock = false;
	}
}
