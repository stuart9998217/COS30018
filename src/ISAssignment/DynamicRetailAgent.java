package ISAssignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class DynamicRetailAgent extends Agent {

	// This retailer agent will charge for 13 hours
	private float chargePeriod = 13;

	// This retailer charges 89.9 KWH every hour
	// TODO: Allow the home to negotiate on this charge for repeated use
	private float supplyCharge = (float) 89.9;

	// this retailer sells its electricity for flat rate of 21.474 cents/ kWh
	private float sellingPrice = (float) 21.474;

	// this reatiler buy electricity with 11.34 cents/ kWh
	private float buyInPrice = (float) 21.474;

	// this retailer sells its electricity for flat rate of 94.4 cents/ kWh for
	// small amount of buying
	private float sellingPriceSmallAmount = (float) 94.4;

	// the amount of electricity offered by the home
	private float kWsOffered = 0;

	// If contract is on
	private boolean ContractOn = false;

	// Transform iteration
	private int iteration = 0;

	private String serviceName = "";
	private List<AID> subscribers = new ArrayList<AID>();

	protected void setup() {
		Object[] args = getArguments();
		serviceName = args[0].toString();
		Random rndinit = new Random();
		sellingPrice = rndinit.nextFloat() * 30 + 5;
		supplyCharge = rndinit.nextFloat() * 60;
		sellingPriceSmallAmount = rndinit.nextFloat() * 50 + 45;
		// Description of service to be registered
		ServiceDescription sd = new ServiceDescription();
		sd.setType("energy-selling");
		sd.setName(serviceName);
		register(sd);

		// Add behaviour that receives messages and reply
		addBehaviour(new RequestProcessingServer());

		// Trigger a random generation if the contract isn't here
		addBehaviour(new TickerBehaviour(this, 30000) {
			protected void onTick() {
				if (!ContractOn) {
					Random rnd = new Random();
					sellingPrice = rnd.nextFloat() * 30 + 5;
					supplyCharge = rnd.nextFloat() * 60;
					buyInPrice = rnd.nextFloat() * 20 + 1;
				} else {
					System.out.println("stop dynamic generation");
				}
			}
		});

		// supply energy to battery agent
		addBehaviour(new TickerBehaviour(this, 10000) {
			protected void onTick() {
				if (ContractOn) {
					ACLMessage msg = new ACLMessage(ACLMessage.CFP);
					msg.addReceiver(new AID("Battery", AID.ISLOCALNAME));
					msg.setContent(String.valueOf(supplyCharge));
					msg.setConversationId("application-generation");
					send(msg);
					iteration++;
					System.out.println(getLocalName() + " sent power Generation to Battery");
					if (iteration == chargePeriod) {
						iteration = 0;
						ContractOn = false;
						// reset the condition of Contract and iteration
					}
				} else {
					System.out.println("Currently has no contract");
				}
			}
		});

	}

	// Method to register the service
	void register(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd); // An agent can register one or more service

		// Register the agent and its services
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	private class RequestProcessingServer extends CyclicBehaviour {

		public void action() {
			ACLMessage msg = receive();
			if (msg != null) {
				// Check if receiving a subscription message
				if (msg.getPerformative() == ACLMessage.INFORM
						&& msg.getConversationId().equals("customer-subscription")) {
					// We know home agent is here
					System.out.println("There is a home agent contacted us.");
					subscribers.add(msg.getSender());
				}

				/*
				 * // Check if receiving a request message if (msg.getPerformative() ==
				 * ACLMessage.CFP) { qty = Integer.parseInt(msg.getContent()); // the quatity of
				 * energy home agent wants to buy
				 * 
				 * // Create a propose reply message that content the price Random rnd = new
				 * Random(); price = rnd.nextInt(20 + 1) + 15; // random number discount =
				 * price; // no discount here
				 * 
				 * //Send proposal back to home agent ACLMessage reply = msg.createReply();
				 * reply.setPerformative(ACLMessage.PROPOSE);
				 * reply.setContent(String.valueOf(price)); myAgent.send(reply);
				 * 
				 * }
				 */

				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-buying")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the buying price");
					ACLMessage reply = msg.createReply();
					if (ContractOn) {

						reply.setContent(Float.toString(0));
						reply.setPerformative(ACLMessage.REFUSE);
					} else {

						reply.setContent(Float.toString(supplyCharge));
						reply.setPerformative(ACLMessage.PROPOSE);
					}
					send(reply);

				}
				// Send back the price per unit
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-price")) {
					System.out.println(
							msg.getSender().getLocalName() + " has requested the buying price per units of power");

					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(sellingPrice));
					reply.setPerformative(ACLMessage.PROPOSE);
					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-period")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the supply period");

					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(chargePeriod));
					reply.setPerformative(ACLMessage.PROPOSE);
					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-confirm")) {
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					confirm.setContent("This offer apply: " + String.valueOf(supplyCharge) + " with price "
							+ String.valueOf(sellingPrice) + " per unit in " + String.valueOf(chargePeriod)
							+ " hours with total price " + String.valueOf(supplyCharge * sellingPrice * chargePeriod));

					myAgent.send(confirm);
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");
					System.out.println("Total price: " + supplyCharge * sellingPrice * chargePeriod);
					ContractOn = true;
				}

				if (msg.getPerformative() == ACLMessage.REFUSE && msg.getConversationId().equals("refuse")) {
					System.out.println("Agent " + getLocalName() + ": Action failed");
				}

				// this deal with energy selling from home agent
				// this agent provide energy buying services
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-selling")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the selling price");

					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(buyInPrice));
					reply.setPerformative(ACLMessage.PROPOSE);
					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-selling-confirm")) {
					float buyingamount = Float.parseFloat(msg.getContent());
					kWsOffered = buyingamount;
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					confirm.setContent("This offer buy In " + String.valueOf(buyingamount) + " with price "
							+ String.valueOf(buyInPrice) + ". Total price:" + buyingamount * buyInPrice);

					myAgent.send(confirm);
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");
					System.out.println("Total price usage in buying : " + buyingamount * buyInPrice);
					addBehaviour(new EnergyBuyInProcess());
				}

				// this deal with small amount of energy buying
				// high price low supply for one time
				if (msg.getPerformative() == ACLMessage.CFP
						&& msg.getConversationId().equals("energy-buying-small-amount")) {
					System.out.println(
							msg.getSender().getLocalName() + " has requested the buying price in small amount buying");
					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(sellingPriceSmallAmount));
					reply.setPerformative(ACLMessage.PROPOSE);

					send(reply);

				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-confirm-small-amount")) {
					float buyingamount = Float.parseFloat(msg.getContent());
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					confirm.setContent("This offer apply: " + buyingamount + " with price "
							+ String.valueOf(sellingPriceSmallAmount) + " per unit." + "Total price: "
							+ sellingPriceSmallAmount * buyingamount);

					myAgent.send(confirm);
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");
					System.out.println("Total price: " + sellingPriceSmallAmount * buyingamount);
				}

			} else {
				block();
			}
		}
	}

	// this process will try to ask battery for energy every 1 second when fridge is
	// 'open'
	// fridge is always 'open'
	private class EnergyBuyInProcess extends Behaviour {
		private MessageTemplate mt;
		private String state = "BuyIn";

		public void action() {

			switch (state) {
			case "BuyIn":
				// ask battery give energy
				ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);

				cfpMsg.addReceiver(new AID("Battery", AID.ISLOCALNAME));
				cfpMsg.setContent(String.valueOf(kWsOffered)); // how much energy need
				cfpMsg.setConversationId("application-require-energy");
				cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
				myAgent.send(cfpMsg);
				// Prepare the message template to get proposals
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("application-require-energy"),
						MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith()));

				state = "Check";
				System.out.println("Home sells us " + kWsOffered + " khw power");
				break;
			case "Check":
				ACLMessage response = myAgent.receive(mt);
				if (response != null) {

					// Response received
					if (response.getPerformative() == ACLMessage.PROPOSE) {
						String success = response.getContent();
						if (success.equals("yes")) {
							state = "done";
							kWsOffered = 0;
						} else {
							state = "done";
							kWsOffered = 0;
						}
					}

				} else {
					block();
				}
				break;

			}

		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return state == "done";
		}

	}

	// Method to send terminated notification to subscribers and de-register the
	// service (on take down)
	protected void takeDown() {
		try {
			// Send notification
			ACLMessage noti = new ACLMessage(ACLMessage.INFORM);
			noti.setConversationId("retailer-terminated");
			noti.setContent("went bankrupt");
			for (AID receiver : subscribers) {
				noti.addReceiver(receiver);
			}
			send(noti);

			// De-register from DF agent
			DFService.deregister(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(getAID().getName() + " terminated");
	}

}
