
/*
	 InDemandRetailerAgent has very low Charge with cheap price
	 but if you go over your usage and you want to extend contract
	 you will be charged a very high penalty rate.
*/

package ISAssignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class InDemandRetailerAgent extends Agent {
	// this retailer send information for these customer
	private List<AID> subscribers = new ArrayList<AID>();
	// this retailer agent has such name
	private String serviceName = "";

	// this retailer send information for these customer
	private Map<AID, Integer> Customer = new HashMap<AID, Integer>();

	// this retailer has a low initial rate of 17.593 cents/ kWh
	private float lowSellingPrice = (float) 17.593;

	// this retailer has a high penalty rate of 62.666 cents/ kWh
	private float highloSellingPrice = (float) 62.666;

	// this retailer sells its electricity for flat rate
	private float buyInPrice = (float) 12.6;

	// this retailer charges 46.42 cents every time it is used by the home
	// TODO: Allow the home to negotiate on this charge for repeated use
	private float supplyCharge = (float) 46.42;

	// this retailer sells its electricity for flat rate of 94.4 cents/ kWh for
	// small amount of buying
	private float sellingPriceSmallAmount = (float) 121.23;

	// this is the discount can be provided to win a contract
	private float discount = 1;

	// This retailer agent will charge for 3 hours
	private float chargePeriod = 6;

	// Extend supply with HighloChargePeriod
	private float HighlochargePeriod = 4;

	// the amount of electricity offered by the home
	private float kWsOffered = 0;

	// If contract is on
	private boolean ContractOn = false;

	// an extra Contract
	private boolean ExtraContract = false;

	// Transform iteration
	private int iteration = 0;
	private int iteration_extra = 0;

	protected void setup() {
		// Only 1 argument is the local name of the home agent
		Object[] args = getArguments();
		serviceName = args[0].toString();
		// Description of service to be registered
		ServiceDescription sd = new ServiceDescription();

		sd.setType("energy-selling");
		sd.setName(serviceName);
		register(sd);

		System.out.println("Agent " + getLocalName() + " waiting for requests...");
		/*
		 * MessageTemplate template = MessageTemplate.and(
		 * MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
		 * MessageTemplate.MatchPerformative(ACLMessage.REQUEST) );
		 */
		addBehaviour(new RequestProcessingServer());

		addBehaviour(new TickerBehaviour(this, 10000) {
			protected void onTick() {
				if (ExtraContract) {
					ACLMessage msg = new ACLMessage(ACLMessage.CFP);
					msg.addReceiver(new AID("Battery", AID.ISLOCALNAME));
					msg.setContent(String.valueOf(supplyCharge));
					msg.setConversationId("application-generation");
					send(msg);
					iteration_extra++;
					System.out.println(getLocalName() + " sent power Generation to Battery");
					if (iteration_extra == HighlochargePeriod) {
						iteration_extra = 0;
						ExtraContract = false;
						// reset the condition of Contract and iteration
					}
				} else {
					System.out.println("Currently has no extra contract");
				}
			}
		});

		addBehaviour(new TickerBehaviour(this, 10000) {
			protected void onTick() {
				if (ContractOn) {
					ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
					msg.addReceiver(new AID("Battery", AID.ISLOCALNAME));
					msg.setContent(String.valueOf(supplyCharge));
					msg.setConversationId("application-generation");
					send(msg);
					iteration++;
					System.out.println(getLocalName() + " sent power Generation to Battery");
					if (iteration == chargePeriod) {
						iteration = 0;
						ContractOn = false;
						// reset the condition of Contract and iteration
					}
				} else {
					System.out.println("Currently has no contract");
				}
			}
		});
		/*
		 * addBehaviour(new AchieveREResponder(this, template) { protected ACLMessage
		 * prepareResponse(ACLMessage request) throws NotUnderstoodException,
		 * RefuseException { System.out.println("Agent " + getLocalName() +
		 * ": REQUEST received from " + request.getSender().getName() + ". Action is " +
		 * request.getContent()); if (checkAction(request)) { // We agree to perform the
		 * action. Note that in the FIPA-Request // protocol the AGREE message is
		 * optional. Return null if you // don't want to send it.
		 * System.out.println("Agent " + getLocalName() + ": Agree"); ACLMessage agree =
		 * request.createReply(); agree.setPerformative(ACLMessage.AGREE); return agree;
		 * } else { // We refuse to perform the action System.out.println("Agent " +
		 * getLocalName() + ": Refuse"); throw new RefuseException("check-failed"); } }
		 * 
		 * protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage
		 * response) throws FailureException { if (performAction(request)) {
		 * System.out.println("Agent " + getLocalName() +
		 * ": Action successfully performed"); ACLMessage inform =
		 * request.createReply(); inform.setPerformative(ACLMessage.INFORM); return
		 * inform; } else { System.out.println("Agent " + getLocalName() +
		 * ": Action failed"); throw new FailureException("unexpected-error"); } } } );
		 */
	}

	private class RequestProcessingServer extends CyclicBehaviour {

		public void action() {
			ACLMessage msg = receive();
			if (msg != null) {
				// Check if receiving a subscription message
				if (msg.getPerformative() == ACLMessage.INFORM
						&& msg.getConversationId().equals("customer-subscription")) {
					// We know home agent is here
					System.out.println("There is a home agent contacted us.");
					subscribers.add(msg.getSender());
					Customer.put(msg.getSender(), 0);

				}
				// Home agent should send message "energy-buying"
				// Checking home request
				// Send back how much energy can provide
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-buying")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the buying price");
					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(supplyCharge));
					reply.setPerformative(ACLMessage.PROPOSE);

					send(reply);

				}
				// Send back the price per unit
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-price")) {
					System.out.println(
							msg.getSender().getLocalName() + " has requested the buying price per units of power");

					ACLMessage reply = msg.createReply();
					if (ContractOn) {
						// if user extend the old contract, they have to pay more
						reply.setContent(Float.toString(highloSellingPrice * discount));
						reply.setPerformative(ACLMessage.PROPOSE);
					}
					if (ContractOn && ExtraContract) {
						reply.setContent(Float.toString(0));
						reply.setPerformative(ACLMessage.REFUSE);
					} else {
						reply.setContent(Float.toString(lowSellingPrice * discount));
						reply.setPerformative(ACLMessage.PROPOSE);
					}

					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-period")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the supply period");

					ACLMessage reply = msg.createReply();
					if (ContractOn) {
						// if user extend the old contract, there will be 4 extra hour with high price
						// to be added
						reply.setContent(Float.toString(HighlochargePeriod * discount));
						reply.setPerformative(ACLMessage.PROPOSE);
					} else {
						reply.setContent(Float.toString(chargePeriod * discount));
						reply.setPerformative(ACLMessage.PROPOSE);
					}

					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-confirm")) {
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					if (ContractOn) {
						// if user extend the old contract, there will be 4 extra hour with high price
						// to be added
						confirm.setContent(
								Float.toString(supplyCharge * highloSellingPrice * HighlochargePeriod * discount));
						ExtraContract = true;
						System.out.println(
								"Total price: " + supplyCharge * highloSellingPrice * HighlochargePeriod * discount);
						System.out.println("This offer is extended");
						System.out.println("This offer is extend with: " + String.valueOf(supplyCharge) + "(discount:"
								+ discount + ")" + " with price " + String.valueOf(highloSellingPrice) + " per unit in "
								+ String.valueOf(HighlochargePeriod) + " hours with total price "
								+ String.valueOf(supplyCharge * highloSellingPrice * HighlochargePeriod * discount));
						
						Gui.updateScrollPaneText(
								msg.getSender().getLocalName() + " has agreed \nto extend " + getLocalName() + 
								"'s contract offer to buy \n" + String.valueOf(supplyCharge) + " kWs, at "
								+ String.valueOf(highloSellingPrice) + " a kW, with a discount of \\n" + discount
								+ " for " + String.valueOf(HighlochargePeriod) + " hours. The total cost is $" 
								+ String.valueOf(supplyCharge * highloSellingPrice * HighlochargePeriod * discount) + "\n"
						);
					} else {
						// new contract is cheap and short

						confirm.setContent(String.valueOf(supplyCharge * lowSellingPrice * chargePeriod * discount));
						System.out.println("Total price: " + supplyCharge * lowSellingPrice * chargePeriod * discount);
						System.out.println("This offer apply: " + String.valueOf(supplyCharge) + "(discount:" + discount
								+ ")" + " with price " + String.valueOf(lowSellingPrice) + " per unit in "
								+ String.valueOf(chargePeriod) + " hours with total price "
								+ String.valueOf(supplyCharge * lowSellingPrice * chargePeriod * discount));
						
						Gui.updateScrollPaneText(
								msg.getSender().getLocalName() + " has agreed to extend " + getLocalName() + 
								"'s contract offer to buy \n" + String.valueOf(supplyCharge) + " kWs, at "
								+ String.valueOf(lowSellingPrice) + " a kW, with a discount of " + discount
								+ " for " + String.valueOf(chargePeriod) + " hours. \nThe total cost is $" 
								+ String.valueOf(supplyCharge * lowSellingPrice * chargePeriod * discount) + "\n"
						);
						
						ContractOn = true;
					}
					myAgent.send(confirm);
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");

				}

				/**
				 * if retailer agent been refused too much times, it will trigger a discount
				 * algorithm to make its contract more attractive
				 */

				if (msg.getPerformative() == ACLMessage.REFUSE && msg.getConversationId().equals("refuse")) {
					System.out.println("Agent " + getLocalName() + ": Action failed");
					Integer i = Customer.get(msg.getSender()) + 1;
					Customer.replace(msg.getSender(), i);
					if (i > 2 && i <= 20) {
						discount = (float) (1.0 - i / 100);
					} else if (i > 20) {
						discount = (float) 0.76;
					} else {
						discount = (float) 1.0;
					}
				}

				// this deal with energy selling from home agent
				// this agent provide energy buying services
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-selling")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the selling price");

					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(buyInPrice));
					reply.setPerformative(ACLMessage.PROPOSE);
					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-selling-confirm")) {
					float buyingamount = Float.parseFloat(msg.getContent());
					kWsOffered = buyingamount;
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					confirm.setContent("This offer buy In " + String.valueOf(buyingamount) + " with price "
							+ String.valueOf(buyInPrice) + ". Total price usage in buying : "
							+ buyingamount * buyInPrice);

					myAgent.send(confirm);
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");
					System.out.println("Total price usage in buying : " + buyingamount * buyInPrice);
					addBehaviour(new EnergyBuyInProcess());
				}

				// this deal with small amount of energy buying
				// high price low supply for one time
				// small amount of buying has no discount
				if (msg.getPerformative() == ACLMessage.CFP
						&& msg.getConversationId().equals("energy-buying-small-amount")) {
					System.out.println(
							msg.getSender().getLocalName() + " has requested the buying price in small amount buying");
					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(sellingPriceSmallAmount));
					reply.setPerformative(ACLMessage.PROPOSE);

					send(reply);

				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-confirm-small-amount")) {
					float buyingamount = Float.parseFloat(msg.getContent());
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					confirm.setContent(Float.toString(sellingPriceSmallAmount * buyingamount));

					myAgent.send(confirm);
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");
					System.out.println("Total price: " + sellingPriceSmallAmount * buyingamount);
					System.out.println("This offer apply: " + buyingamount + " with price "
							+ String.valueOf(sellingPriceSmallAmount) + " per unit. " + "Total price: "
							+ sellingPriceSmallAmount * buyingamount);
					
					Gui.updateScrollPaneText(
							msg.getSender().getLocalName() + " has accepted " + getLocalName() + 
							"'s contract offer to buy " + String.valueOf(buyingamount) + " kWs, at "
							+ String.valueOf(sellingPriceSmallAmount) + " a kW, for a total \ncost of $"
							+ String.valueOf(sellingPriceSmallAmount * buyingamount) + "\n"
					);
				}
			} else {
				block();
			}
		}
	}

	// Method to register the service
	void register(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd); // An agent can register one or more services

		// Register the agent and its services
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	// this process will try to ask battery for energy every 1 second when fridge is
	// 'open'
	// fridge is always 'open'
	private class EnergyBuyInProcess extends Behaviour {
		private MessageTemplate mt;
		private String state = "BuyIn";

		public void action() {

			switch (state) {
			case "BuyIn":
				// ask battery give energy
				ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);

				cfpMsg.addReceiver(new AID("Battery", AID.ISLOCALNAME));
				cfpMsg.setContent(String.valueOf(kWsOffered)); // how much energy need
				cfpMsg.setConversationId("application-require-energy");
				cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
				myAgent.send(cfpMsg);
				// Prepare the message template to get proposals
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("application-require-energy"),
						MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith()));

				state = "Check";
				System.out.println("Home sells us " + kWsOffered + " khw power");
				break;
			case "Check":
				ACLMessage response = myAgent.receive(mt);
				if (response != null) {

					// Response received
					if (response.getPerformative() == ACLMessage.PROPOSE) {
						String success = response.getContent();
						if (success.equals("yes")) {
							state = "done";
							kWsOffered = 0;
						} else {
							state = "done";
							kWsOffered = 0;
						}
					}

				} else {
					block();
				}
				break;

			}

		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return state == "done";
		}

	}

	// Method to de register the service (on take down)
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (Exception e) {
		}
	}

	/*
	 * private boolean checkAction(ACLMessage request) { return
	 * (request.getSender().getName() == "home"); }
	 * 
	 * private boolean performAction(ACLMessage request) { if (msg != null) { if
	 * (msg.getConversationId().equals("buy-electricity")) {
	 * System.out.println(msg.getSender().getLocalName() +
	 * " has requested the buying price of " + msg.getContent() + " kWs");
	 * kWsRequested = Float.parseInt(msg.getContent());
	 * 
	 * // the price that the home will pay if they don't exceed the amount of
	 * requested electricity float price = supplyCharge + kWsRequested *
	 * lowSellingPrice;
	 * 
	 * reply.addReceiver(new AID(args[0].toString(), AID.ISLOCALNAME));
	 * reply.setContent(price);
	 * reply.setConversationId("inDemandRetailer-sellingPrice"); send(reply);
	 * 
	 * return true; } else if (msg.getConversationId().equals("buy-penaltyRate")) {
	 * System.out.println(msg.getSender().getLocalName() +
	 * " has requested the penalty rate if it exceeds " + msg.getContent() +
	 * " kWs");
	 * 
	 * // the penalty rate, which is 62.666 per kW exceeded reply.addReceiver(new
	 * AID(args[0].toString(), AID.ISLOCALNAME));
	 * reply.setContent(highloSellingPrice);
	 * reply.setConversationId("inDemandRetailer-penaltyRate"); send(reply);
	 * 
	 * return true; } else if (msg.getConversationId().equals("sell-electricity")) {
	 * System.out.println(msg.getSender().getLocalName() +
	 * " has requested the selling price of " + msg.getContent() + " kWs");
	 * kWsOffered = Float.parseInt(msg.getContent());
	 * 
	 * ACLMessage reply = new ACLMessage(ACLMessage.INFORM); reply.addReceiver(new
	 * AID(args[0].toString(), AID.ISLOCALNAME)); reply.setContent(sellingPrice);
	 * reply.setConversationId("inDemandRetailer-buyingPrice"); send(reply);
	 * 
	 * return true; } else { return false; } } else { return false; } }
	 */
}