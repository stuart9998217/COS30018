// @author Timothy Quill (100997474)

package ISAssignment;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import jade.Boot;

public class Main {
	static Clock clock = new Clock();
	static Gui window = new Gui();

	public static void main(String[] args) {
		// launch gui
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		Timer timer = new Timer("Timer");

		long delay = 1000L;
		long period = 1000L;
		timer.scheduleAtFixedRate(checkStart, delay, period);
	}

	static TimerTask checkStart = new TimerTask() {
		@Override
		public void run() {
			if (window.scenario == "scenario 1") {
				// launch scenario 1

				String[] param = new String[3];

				// Launch Home agent
				param[0] = "-gui";
				param[1] = "-agents";
				param[2] = "Home:ISAssignment.HomeAgent()";

				Boot.main(param);
				System.out.println("Launched Home agent");

				// Launch Battery agent
				param[0] = "-container";
				param[1] = "-agents";
				param[2] = "Battery:ISAssignment.BatteryAgent(\"Home\",10)";

				Boot.main(param);
				System.out.println("Launched Battery agent");

				// Launch Retailer agents
				param[0] = "-container";
				param[1] = "-agents";
				param[2] = "Retailer1:ISAssignment.FixedPriceRetailerAgent(\"FixedPrice\");Retailer2:ISAssignment.BulkRetailerAgent(\"Bulk\");Retailer3:ISAssignment.InDemandRetailerAgent(\"InDemand\")";

				Boot.main(param);
				System.out.println("Launched Retailer agents");

				clock.resetClock();
				clock.startClock();
				window.scenario = "";
			} else if (window.scenario == "scenario 2") {
				// launch scenario 2

				String[] param = new String[3];

				// Launch Home agent
				param[0] = "-gui";
				param[1] = "-agents";
				param[2] = "Home:ISAssignment.HomeAgent()";

				Boot.main(param);
				System.out.println("Launched Home agent");

				// Launch Battery agent
				param[0] = "-container";
				param[1] = "-agents";
				param[2] = "Battery:ISAssignment.BatteryAgent(\"Home\",5)";

				Boot.main(param);
				System.out.println("Launched Battery agent");

				// Launch Heating agent
				param[0] = "-container";
				param[1] = "-agents";
				param[2] = "Heating:ISAssignment.HeatingAgent(\"Heating\")";

				Boot.main(param);
				System.out.println("Launched Heating agent");

				// Launch Car agent
				param[0] = "-container";
				param[1] = "-agents";
				param[2] = "Car:ISAssignment.ElectricCarAgent(\"Car\")";

				Boot.main(param);
				System.out.println("Launched Car agent");

				// Launch Lighting agent
				param[0] = "-container";
				param[1] = "-agents";
				param[2] = "Lighting:ISAssignment.LightingAgent(\"light\")";

				Boot.main(param);
				System.out.println("Launched Lighting agent");

				// Launch Retailer agents
				param[0] = "-container";
				param[1] = "-agents";
				param[2] = "FixedPriceRetailer:ISAssignment.FixedPriceRetailerAgent(\"FixedPrice\");BulkRetailer:ISAssignment.BulkRetailerAgent(\"Bulk\");InDemandRetailer:ISAssignment.InDemandRetailerAgent(\"InDemand\");LimitedRetailer:ISAssignment.LimitedRetailerAgent(\\\"Limited\\\");";

				Boot.main(param);
				System.out.println("Launched Retailer agents");

				// sourced from
				// https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
				// read appliance data
				URL url = Main.class.getClass().getResource("/cleaned_data.csv");
				File file = new File(url.getPath());
				BufferedReader br = null;
				String line = "";
				String cvsSplitBy = ",";

				try {
					br = new BufferedReader(new FileReader(file));
					while ((line = br.readLine()) != null) {

						// use comma as separator
						String[] data = line.split(cvsSplitBy);

						try {
							ISAssignment.ElectricCarAgent.setCurrentUsage(Float.parseFloat(data[1]));
							ISAssignment.LightingAgent.setCurrentUsage(Float.parseFloat(data[2]));
							ISAssignment.HeatingAgent.setCurrentUsage(Float.parseFloat(data[3]));
							ISAssignment.SolarPanelAgent.setCurrentCharge(Float.parseFloat(data[4]));
						} catch (NumberFormatException ex) { // handle your exception
							System.out.println("Not a float!");
						}
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} finally {
					if (br != null) {
						try {
							br.close();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}

				clock.resetClock();
				clock.startClock();
				window.scenario = "";
			} else {
				clock.update();
				String td = clock.getTimeDate();
				window.setTimeDate(td);
			}
		}
	};
}
