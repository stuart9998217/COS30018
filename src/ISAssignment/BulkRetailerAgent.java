
/*
	Offers a low Charge, but you have to buy a certain amount of power at one time
	Supply such amount energy in each period during a time
	Don't accept extend offer when it has a contract
*/

package ISAssignment;

import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class BulkRetailerAgent extends Agent {
	// this retailer send information for these customer
	private List<AID> subscribers = new ArrayList<AID>();
	// this retailer agent has such name
	private String serviceName = "";
	// this retailer sells its electricity for flat rate of 21.474 cents/ kWh
	private float sellingPrice = (float) 21.474;

	// this retailer sells its electricity for flat rate
	private float buyInPrice = (float) 9.44;

	// This retailer charges 77.628 KWH every hour
	private float supplyCharge = (float) 77.628;

	// this retailer sells its electricity for flat rate of 94.4 cents/ kWh for
	// small amount of buying
	private float sellingPriceSmallAmount = (float) 73.62;

	// This retailer agent will charge for 13 hours
	private float chargePeriod = 13;

	// the amount of electricity offered by the home
	private float kWsOffered = 0;

	// If contract is on
	private boolean ContractOn = false;

	// Transform iteration
	private int iteration = 0;

	protected void setup() {
		// Only 1 argument is the local name of the home agent
		Object[] args = getArguments();
		serviceName = args[0].toString();
		// Description of service to be registered
		ServiceDescription sd = new ServiceDescription();

		sd.setType("energy-selling");
		sd.setName(serviceName);
		register(sd);

		System.out.println("Agent " + getLocalName() + " waiting for requests...");
		/*
		 * MessageTemplate template = MessageTemplate.and(
		 * MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
		 * MessageTemplate.MatchPerformative(ACLMessage.REQUEST) );
		 */

		addBehaviour(new RequestProcessingServer());

		// supply energy to battery agent
		addBehaviour(new TickerBehaviour(this, 10000) {
			protected void onTick() {
				if (ContractOn) {
					ACLMessage msg = new ACLMessage(ACLMessage.CFP);
					msg.addReceiver(new AID("Battery", AID.ISLOCALNAME));
					msg.setContent(String.valueOf(supplyCharge));
					msg.setConversationId("application-generation");
					send(msg);
					iteration++;
					System.out.println(getLocalName() + " sent power Generation to Battery");
					if (iteration == chargePeriod) {
						iteration = 0;
						ContractOn = false;
						// reset the condition of Contract and iteration
					}
				} else {
					System.out.println("Currently has no contract");
				}
			}
		});

		/*
		 * addBehaviour(new AchieveREResponder(this, template) { protected ACLMessage
		 * prepareResponse(ACLMessage request) throws NotUnderstoodException,
		 * RefuseException { System.out.println("Agent " + getLocalName() +
		 * ": REQUEST received from " + request.getSender().getName() + ". Action is " +
		 * request.getContent()); if (checkAction(request)) { // We agree to perform the
		 * action. Note that in the FIPA-Request // protocol the AGREE message is
		 * optional. Return null if you // don't want to send it.
		 * System.out.println("Agent " + getLocalName() + ": Agree"); ACLMessage agree =
		 * request.createReply(); agree.setPerformative(ACLMessage.AGREE); return agree;
		 * } else { // We refuse to perform the action System.out.println("Agent " +
		 * getLocalName() + ": Refuse"); throw new RefuseException("check-failed"); } }
		 * 
		 * protected ACLMessage prepareResultNotification(ACLMessage request) throws
		 * FailureException { if (performAction(request)) { System.out.println("Agent "
		 * + getLocalName() + ": Action successfully performed"); ACLMessage inform =
		 * request.createReply(); inform.setPerformative(ACLMessage.INFORM); return
		 * inform; } else { System.out.println("Agent " + getLocalName() +
		 * ": Action failed"); throw new FailureException("unexpected-error"); } } } );
		 */
	}

	private class RequestProcessingServer extends CyclicBehaviour {

		public void action() {
			ACLMessage msg = receive();
			if (msg != null) {
				// Check if receiving a subscription message
				if (msg.getPerformative() == ACLMessage.INFORM
						&& msg.getConversationId().equals("customer-subscription")) {
					// We know home agent is here
					System.out.println("There is a home agent contacted us.");
					subscribers.add(msg.getSender());
				}
				// Home agent should send message "energy-buying"
				// Checking home request
				// Send back how much energy can provide
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-buying")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the buying price");
					ACLMessage reply = msg.createReply();
					if (ContractOn) {

						reply.setContent(Float.toString(0));
						reply.setPerformative(ACLMessage.REFUSE);
					} else {

						reply.setContent(Float.toString(supplyCharge));
						reply.setPerformative(ACLMessage.PROPOSE);
					}

					send(reply);

				}
				// Send back the price per unit
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-price")) {
					System.out.println(
							msg.getSender().getLocalName() + " has requested the buying price per units of power");

					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(sellingPrice));
					reply.setPerformative(ACLMessage.PROPOSE);
					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-period")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the supply period");

					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(chargePeriod));
					reply.setPerformative(ACLMessage.PROPOSE);
					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-selling-confirm")) {
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					confirm.setContent(Float.toString(supplyCharge * sellingPrice * chargePeriod));
					myAgent.send(confirm);
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");
					System.out.println("This offer apply: " + String.valueOf(supplyCharge) + " with price "
							+ String.valueOf(sellingPrice) + " per unit in " + String.valueOf(chargePeriod)
							+ " hours with total price " + String.valueOf(supplyCharge * sellingPrice * chargePeriod));
					System.out.println("Total price: " + supplyCharge * sellingPrice * chargePeriod);
					ContractOn = true;
					sellingPrice = (float) (sellingPrice * 1.01);
					
					Gui.updateScrollPaneText(
							msg.getSender().getLocalName() + " has accepted \n" + getLocalName() + 
							"'s contract offer to buy \n" + String.valueOf(supplyCharge) + " kWs, at "
							+ String.valueOf(sellingPrice) + " a kW, for a total \ncost of $"
							+ String.valueOf(supplyCharge * sellingPrice * chargePeriod) + "\n"
					);
				}

				/**
				 * if retailer agent been refused too much times, it will trigger a discount
				 * algorithm to make its contract more attractive however everytime is gets a
				 * contract, it will slowly increase its price until it gets a refuse
				 */

				if (msg.getPerformative() == ACLMessage.REFUSE && msg.getConversationId().equals("refuse")) {
					System.out.println("Agent " + getLocalName() + ": Action failed");
					sellingPrice = (float) (sellingPrice * 0.75);

				}

				// this deal with energy selling from home agent
				// this agent provide energy buying services
				if (msg.getPerformative() == ACLMessage.CFP && msg.getConversationId().equals("energy-selling")) {
					System.out.println(msg.getSender().getLocalName() + " has requested the selling price");

					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(buyInPrice));
					reply.setPerformative(ACLMessage.PROPOSE);
					send(reply);
				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-selling")) {
					float buyingamount = Float.parseFloat(msg.getContent());
					kWsOffered = buyingamount;
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					confirm.setContent("This offer buy In " + String.valueOf(buyingamount) + " with price "
							+ String.valueOf(buyInPrice) + ". Total price usage in buying : "
							+ buyingamount * buyInPrice);

					myAgent.send(confirm);
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");
					System.out.println("Total price usage in buying : " + buyingamount * buyInPrice);
					addBehaviour(new EnergyBuyInProcess());
				}

				// this deal with small amount of energy buying
				// high price low supply for one time
				if (msg.getPerformative() == ACLMessage.CFP
						&& msg.getConversationId().equals("energy-buying-small-amount")) {
					System.out.println(
							msg.getSender().getLocalName() + " has requested the buying price in small amount buying");
					ACLMessage reply = msg.createReply();
					reply.setContent(Float.toString(sellingPriceSmallAmount));
					reply.setPerformative(ACLMessage.PROPOSE);

					send(reply);

				}

				if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL
						&& msg.getConversationId().equals("energy-confirm-small-amount")) {
					float buyingamount = Float.parseFloat(msg.getContent());
					ACLMessage confirm = msg.createReply();
					confirm.setPerformative(ACLMessage.CONFIRM);
					confirm.setContent(Float.toString(sellingPriceSmallAmount * buyingamount));

					myAgent.send(confirm);
					System.out.println("Agent " + getLocalName() + ": Action successfully performed");
					System.out.println("Total price: " + sellingPriceSmallAmount * buyingamount);
					System.out.println("This offer apply: " + buyingamount + " with price "
							+ String.valueOf(sellingPriceSmallAmount) + " per unit." + "Total price: "
							+ sellingPriceSmallAmount * buyingamount);
					
					Gui.updateScrollPaneText(
							msg.getSender().getLocalName() + " has accepted \n" + getLocalName() + 
							"'s small-amount offer to buy \n" + buyingamount + " kWs, at "
							+ String.valueOf(sellingPriceSmallAmount) + " a kW, for a total \ncost of $"
							+ String.valueOf(sellingPriceSmallAmount * buyingamount)  + "\n"
					);
				}

			} else {
				block();
			}
		}
	}

	// Method to register the service
	void register(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd); // An agent can register one or more services

		// Register the agent and its services
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	// Method to de register the service (on take down)
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (Exception e) {
		}
	}

	private class EnergyBuyInProcess extends Behaviour {
		private MessageTemplate mt;
		private String state = "BuyIn";

		public void action() {

			switch (state) {
			case "BuyIn":
				// ask battery give energy
				ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);

				cfpMsg.addReceiver(new AID("Battery", AID.ISLOCALNAME));
				cfpMsg.setContent(String.valueOf(kWsOffered)); // how much energy need
				cfpMsg.setConversationId("application-require-energy");
				cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
				myAgent.send(cfpMsg);
				// Prepare the message template to get proposals
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("application-require-energy"),
						MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith()));

				state = "Check";
				System.out.println("Home sells us " + kWsOffered + " khw power");
				break;
			case "Check":
				ACLMessage response = myAgent.receive(mt);
				if (response != null) {

					// Response received
					if (response.getPerformative() == ACLMessage.PROPOSE) {
						String success = response.getContent();
						if (success.equals("yes")) {
							state = "done";
							kWsOffered = 0;
						} else {
							state = "done";
							kWsOffered = 0;
						}
					}

				} else {
					block();
				}
				break;

			}

		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return state == "done";
		}

	}
	/*
	 * private boolean checkAction(ACLMessage request) { return
	 * (request.getSender().getName() == "home"); }
	 */

	/*
	 * private boolean performAction(ACLMessage msg) { if (msg != null) {
	 * 
	 * 
	 * msg.getConversationId().equals("buy-electricity") if (msg.getPerformative()
	 * == ACLMessage.CFP) { System.out.println(msg.getSender().getLocalName() +
	 * " has requested the buying price of " + msg.getContent() +
	 * " units of power"); kWsRequested = Float.parseFloat(msg.getContent());
	 * 
	 * float price = supplyCharge + kWsRequested * sellingPrice; ACLMessage reply =
	 * msg.createReply(); reply.setContent(Float.toString(price));
	 * reply.setPerformative(ACLMessage.PROPOSE); send(reply);
	 * 
	 * return true; }
	 * 
	 * if (msg.getConversationId().equals("buy-minimum")) {
	 * System.out.println(msg.getSender().getLocalName() +
	 * " has requested the minimum amount of kWs it has to purchase");
	 * 
	 * // the penalty rate, which is 62.666 per kW exceeded ACLMessage reply =
	 * msg.createReply(); reply.addReceiver(new AID("home", AID.ISLOCALNAME));
	 * reply.setContent(Float.toString(minKWs));
	 * reply.setConversationId("bulkRetailer-minimum"); send(reply);
	 * 
	 * return true; }
	 * 
	 * if (msg.getConversationId().equals("buy-electricity")) {
	 * System.out.println(msg.getSender().getLocalName() +
	 * " has requested the selling price of " + msg.getContent() +
	 * " units of power"); kWsOffered = Float.parseFloat(msg.getContent());
	 * 
	 * float price = kWsOffered * buyingPrice;
	 * 
	 * ACLMessage reply = new ACLMessage(ACLMessage.INFORM); reply.addReceiver(new
	 * AID("home", AID.ISLOCALNAME)); reply.setContent(Float.toString(price));
	 * reply.setConversationId("bulkRetailer-buyingPrice"); send(reply);
	 * 
	 * return true; }
	 * 
	 * 
	 * 
	 * } else { return false; } }
	 */
}