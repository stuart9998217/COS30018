package ISAssignment;

import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

/**
 * Clock that can tell agents what the time is. Implements a 'watch-time'
 * service. Agents wishing to use this service should send a subscribe message
 * with conversation-id 'watch-time'. Agents may also request the current time
 * by sending a request-ref message with conversation-id 'check-time'.
 *
 * This clock does not track wall-clock time. Rather it increments its current
 * notion of what the time is by some value each second. The time step is
 * initialised to zero, ie. the time never changes. The time step can be changed
 * by sending a propose message with converstaion-id 'set-time-step'. The
 * content of such a message should be a java.time.Duration converted to a
 * string.
 * 
 * @author Stuart Bassett (99998217)
 */
public class ClockAgent extends Agent {

	private Instant current_time;
	private Duration time_step;
	private List<AID> subscribers;

	@Override
	protected void setup() {
		current_time = Instant.now();
		time_step = Duration.ZERO;
		subscribers = new ArrayList<>();

		addBehaviour(new ClockAgentTick(this));
		addBehaviour(new ClockAgentProcessRequests(this));

		ServiceDescription sd = new ServiceDescription();
		sd.setType("watch-time");
		sd.setName(getName());
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		} catch (FIPAException x) {
			x.printStackTrace();
			this.doDelete();
		}
	}

	private void tick() {
		current_time = current_time.plus(time_step);
	}

	public Instant current_instant() {
		// Return a copy of the current time
		return Instant.from(current_time);
	}

	private class ClockAgentTick extends TickerBehaviour {

		private final ClockAgent clock;

		public ClockAgentTick(ClockAgent clock) {
			// Period has to be specified in milliseconds
			super(clock, 1000);
			this.clock = clock;
		}

		/**
		 * Each tick the current time is modified by the time step and sent out to all
		 * subscribers.
		 */
		@Override
		public void onTick() {
			clock.tick();
			final String current_instant = clock.current_instant().toString();
			ACLMessage message = new ACLMessage(ACLMessage.INFORM);
			message.setContent(current_instant);
			message.setConversationId("watch-time");
			message.setSender(clock.getAID());
			for (AID subscriber : clock.subscribers) {
				message.addReceiver(subscriber); // Why is there no addReceivers(List<AID>) ?
			}
			send(message);
		}

	}

	private class ClockAgentProcessRequests extends CyclicBehaviour {

		private final ClockAgent clock;

		public ClockAgentProcessRequests(ClockAgent clock) {
			this.clock = clock;
		}

		/**
		 * Responds to messages as they arrive.
		 * 
		 * @see ClockAgent
		 */
		@Override
		public void action() {
			ACLMessage message;
			while ((message = receive()) != null) {
				ACLMessage reply = message.createReply();
				reply.removeReceiver(clock.getAID()); // For some this agent is retained as the receiver? If someone can
														// explain this to me, that would be great.
				if (message.getPerformative() == ACLMessage.QUERY_REF
						&& message.getConversationId().equals("check-time")) {
					reply.setPerformative(ACLMessage.INFORM);
					reply.setContent(clock.current_instant().toString());
				} else if (message.getPerformative() == ACLMessage.PROPOSE
						&& message.getConversationId().equals("set-time-step")) {
					try {
						Duration d = Duration.parse(message.getContent());
						clock.time_step = d;
						reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
					} catch (DateTimeParseException x) {
						reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
						reply.setContent(x.getParsedString());
					}
				} else if (message.getPerformative() == ACLMessage.SUBSCRIBE
						&& message.getConversationId().equals("watch-time")) {
					clock.subscribers.add(message.getSender());
					reply.setPerformative(ACLMessage.AGREE);
				} else if (message.getPerformative() == ACLMessage.CANCEL
						&& message.getConversationId().equals("watch-time")) {
					clock.subscribers.remove(message.getSender());
					reply.setPerformative(ACLMessage.INFORM);
				} else {
					reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
				}
				send(reply);
			}
			block();
		}

	}
}
