package ISAssignment;

import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

/**
 * Fridge agent is the most simple agent it keep consuming energy don't inform
 * home
 * 
 * @author qiyuan zhu (100858830)
 **/
public class FridgeAgent extends Agent {
	private String serviceName = "";
	private List<AID> subscribers = new ArrayList<AID>();

	protected void setup() {
		Object[] args = getArguments();
		serviceName = args[0].toString();

		// Description of service to be registered
		ServiceDescription sd = new ServiceDescription();
		sd.setType("application");
		sd.setName(serviceName);
		register(sd);

		// Add behaviour that receives messages and reply
		addBehaviour(new RequestProcessingServer());

		// this ticking is try to stimulate the real time energy consuming
		// in this fast ticking it will try to consume energy every 0.1 second by x
		// units if 'open' is true
		addBehaviour(new TickerBehaviour(this, 10000) {
			protected void onTick() {
				addBehaviour(new FridgeProcess());
			}
		});

	}

	// Method to register the service
	void register(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.addServices(sd); // An agent can register one or more service

		// Register the agent and its services
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	private class RequestProcessingServer extends Behaviour {

		public void action() {
			ACLMessage msg = receive();
			if (msg != null) {
				// Check if receiving a subscription message
				if (msg.getConversationId().equals("customer-subscription")) {
					// We know home agent is here
					System.out.println("There is a home agent contacted us.");
					subscribers.add(msg.getSender());
				}

			} else {
				block();
			}
		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return false;
		}

	}

	// this process will try to ask battery for energy every 1 second when fridge is
	// 'open'
	// fridge is always 'open'
	private class FridgeProcess extends Behaviour {

		public void action() {

			// ask battery give energy
			ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);

			cfpMsg.addReceiver(subscribers.get(0));
			cfpMsg.setContent("30"); // how much energy need
			cfpMsg.setConversationId("application-require-energy");
			cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
			myAgent.send(cfpMsg);

			System.out.println("Fridge is working, using 30 unit energy");

		}

		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return true;
		}

	}

	// Method to send terminated notification to subscribers and de-register the
	// service (on take down)
	protected void takeDown() {
		try {
			// Send notification
			ACLMessage noti = new ACLMessage(ACLMessage.INFORM);
			noti.setConversationId("retailer-terminated");
			noti.setContent("went bankrupt");
			for (AID receiver : subscribers) {
				noti.addReceiver(receiver);
			}
			send(noti);

			// De-register from DF agent
			DFService.deregister(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(getAID().getName() + " terminated");
	}

	public boolean done() {
		// TODO Auto-generated method stub
		return false;
	}
}
