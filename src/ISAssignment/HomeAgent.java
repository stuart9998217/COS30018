package ISAssignment;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.TimeUnit;


import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SubscriptionInitiator;
import jade.util.leap.Iterator;


/**
 * 
 * @author Stuart Bassett (9998217)
 */

/**
 * This home agent subscribe for all service (buying/selling energy/solar panel/car/home heating) in DF agent
 * Based on week 6 tutorial
 * And http://jade.tilab.com/doc/tutorials/JADEProgramming-Tutorial-for-beginners.pdf
 * @author qiyuan zhu (100858830)
 **/
/**
 * Functionality general description:
 * In setup, home agent setting the communcation with application/retailer agent
 * also it setting up CyclicBehaviour to handling car charge quest, 
 * and update the knowledge about battery storage
 * it also triggers emergence energy buying in three different ways
 * 1. limited retailer agent buying
 * 2. small amount of one time buying
 * 3. normal contract buying
 * 4. (extra)selling process
 * @author qiyuan zhu (100858830)
 **/

public class HomeAgent extends Agent{
	private Map<AID, String> retailerList = new HashMap<AID, String>();
	private Map<AID, String> applicationList = new HashMap<AID, String>();
	/**
     * Current battery charge.
     */
    private float charge = 0;
    /**
     * temp record of how much energy need to buy.
     */
    private float energyNeed = 0; // for now is 0
    
    public float getEnergyNeed() {
		return energyNeed;
	}

	public void setEnergyNeed() {
		this.energyNeed = 0;
	}
	/**
     * preicationBase will record 24 hours energy usage
     * and use these predication to see if there are some energy is needed
     * it will record the use of heating, fridge and lighting
     * because the battery storage is used to support basic application usage 
     * will not record car, since the charge of it is not frequently but high amount in a period of time
     */
    private float predicationBase = 0;     
  
    private float LimitedBuyingQyt = 0;
    
    
protected void setup () {
	Timer timer = new Timer();  
    timer.schedule(new SolarGenerationPredication(), 1000, 24000);  
    
	String serviceName = "energy-selling";
	String application = "application";
		// Build the description used as template for the subscription
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription templateSd = new ServiceDescription();
		templateSd.setType(serviceName);
		template.addServices(templateSd);
		
		DFAgentDescription template2 = new DFAgentDescription();
		ServiceDescription templateSd2 = new ServiceDescription();
		templateSd2.setType(application);
		template2.addServices(templateSd2);
		
		
		SearchConstraints sc = new SearchConstraints();
		SearchConstraints sc2 = new SearchConstraints();
		// Do a search asking DF return signed agent
		// We want to receive 3 results at most
		sc.setMaxResults(new Long(3));
		sc2.setMaxResults(new Long(5));
		
		// Start subscription for any retailers provide "energy-selling" service
			addBehaviour(new SubscriptionInitiator(this, DFService.createSubscriptionMessage(this, getDefaultDF(), template, sc)) {
					protected void handleInform(ACLMessage inform) {
						System.out.println("Agent " + getLocalName() + ": Notification received from DF");
						try {
							DFAgentDescription[] results = DFService.decodeNotification(inform.getContent());
							if (results.length > 0) {
								for (int i = 0; i < results.length; ++i) {
									DFAgentDescription dfd = results[i];
									AID retailer = dfd.getName();
									
									// In this case, every retailer agent will only have 1 service (only one element in the iterator)
									// Just looking for "energy-selling" service
									Iterator it = dfd.getAllServices();
									while (it.hasNext()) {
										ServiceDescription sd = (ServiceDescription)it.next();
										if (sd.getType().equals(serviceName)) {
											System.out.println("Agent offering Energy-Selling Service found");
											System.out.println("Service \"" + sd.getName() + "\" is provided by agent " + retailer.getName());
											
											// Add this provider to list
											retailerList.put(retailer, sd.getName());
											
											// Send a notification to let that retailer knows we subscribed
											ACLMessage noti = new ACLMessage(ACLMessage.INFORM);
											noti.addReceiver(retailer);
											noti.setContent("subscribed");
											noti.setConversationId("customer-subscription");
											send(noti);
										}
									}
								}
							}
							System.out.println();
						}
						catch (FIPAException fe) {
							fe.printStackTrace();
						}
					}
				});
				
			// Start subscription for any retailers provide "application" service
						addBehaviour(new SubscriptionInitiator(this, DFService.createSubscriptionMessage(this, getDefaultDF(), template2, sc2)) {
								protected void handleInform(ACLMessage inform) {
									System.out.println("Agent " + getLocalName() + ": Notification received from DF");
									try {
										DFAgentDescription[] results = DFService.decodeNotification(inform.getContent());
										if (results.length > 0) {
											for (int i = 0; i < results.length; ++i) {
												DFAgentDescription dfd = results[i];
												AID agent = dfd.getName();
												
												// In this case, every agent will only have 1 service (only one element in the iterator)
												// Just looking for "application" service
												Iterator it = dfd.getAllServices();
												while (it.hasNext()) {
													ServiceDescription sd = (ServiceDescription)it.next();
													if (sd.getType().equals(application)) {
														System.out.println("Agent offering application found");
														System.out.println("Service \"" + sd.getName() + "\" is provided by agent " + agent.getName());
														
														// Add this provider to list
														applicationList.put(agent, sd.getName());
														
														// Send a notification to let that agent knows we subscribed
														ACLMessage noti = new ACLMessage(ACLMessage.INFORM);
														noti.addReceiver(agent);
														noti.setContent("subscribed");
														noti.setConversationId("customer-subscription");
														send(noti);
													}
												}
											}
										}
										System.out.println();
									}
									catch (FIPAException fe) {
										fe.printStackTrace();
									}
								}
							});	
		// Open message receive cyclic behaviour to receive message match id "application-battery" from battery agents
		// Or match id "terminated" from other agents
				addBehaviour(new CyclicBehaviour() {
					public void action() {
						
						MessageTemplate msgTmp = MessageTemplate.or(MessageTemplate.MatchConversationId("application-battery"),
												MessageTemplate.MatchConversationId("terminated"));
						ACLMessage msg = receive(msgTmp);
						
						if (msg != null) {
							
							if (msg.getConversationId().equals("application-battery")&& msg.getPerformative()== ACLMessage.CFP) {
								
								System.out.println(msg.getSender().getLocalName() + " currently has " + msg.getContent() + " units of power");
								charge = Float.parseFloat(msg.getContent());
								
							}
							else {
								System.out.println("termination check");
								// This case must be the termination message from other agents
								// Here is retailer agent
								// Remove the sender retailer from list
								if (retailerList.containsKey(msg.getSender())) { 
									// If that sender actually in our list
									retailerList.remove(msg.getSender());
									System.out.println(msg.getSender().getLocalName() + " " + msg.getContent());
								}
							}
						}
						else {
							block();
						}
					}
				});	
				
				
				//get warning from battery agent if they storage and usage have issues
				addBehaviour(new CyclicBehaviour() {
					public void action() {
						
						MessageTemplate msgTmp = MessageTemplate.or(MessageTemplate.MatchConversationId("application-battery-warning"),
								MessageTemplate.MatchConversationId("application-battery-warning-full"));
								
						ACLMessage msg = receive(msgTmp);
						if (msg != null && msg.getPerformative()== ACLMessage.CFP) {
							System.out.println("receive from battery");
							if (msg.getConversationId().equals("application-battery-warning")) {	
								System.out.println(msg.getSender().getLocalName() + " currently has " + msg.getContent() + " units of power. There is a warning for fast usage and low storage.");
								charge = Float.parseFloat(msg.getContent());
								myAgent.addBehaviour(new StartNormalBuyingRequest());
								
							}else if(msg.getConversationId().equals("application-battery-warning-full")) {
								System.out.println(msg.getSender().getLocalName() + " currently has " + msg.getContent() + " units of power. There is a warning for storage is full.");
								charge = Float.parseFloat(msg.getContent());
								myAgent.addBehaviour(new StartSellingRequest());
							}else {
								System.out.println("something error");
							}
						}
						else {
							block();
						}
					}
				});	
				
				// handle car charge quest 
				addBehaviour(new CyclicBehaviour() {
					public void action() {
						
						MessageTemplate msgTmp = MessageTemplate.MatchConversationId("application-car-charge");
						ACLMessage msg = receive(msgTmp);
						if (msg != null) {
							
							if (msg.getConversationId().equals("application-car-charge")) {	
								System.out.println(msg.getSender().getLocalName() + " needs " + msg.getContent() + " units of power.");
								ACLMessage reply = msg.createReply();							
								if(charge >  Integer.parseInt(msg.getContent())) {
									reply.setContent("yes");
								}else {
									reply.setContent("no");
								}						
								send(reply);													
							}else {
								System.out.println("something error");
							}
						}else {
							block();
						}
					}
				});	
				
				// handle limited retailer agent
				// buy if the storage isn't enough for next day as predication
				addBehaviour(new CyclicBehaviour() {
					public void action() {
						
						MessageTemplate msgTmp = MessageTemplate.MatchConversationId("limited-charge");
						ACLMessage msg = receive(msgTmp);
						if (msg != null) {
							if (msg.getConversationId().equals("limited-charge")) {	
								String temp = msg.getContent();
								if(temp.equals("open")) {
									System.out.println("retailer agent is online");
									// init a limited buying process
									float predicationGeneration = SolarGenerationPredication.getPredication();
									if(predicationBase-predicationGeneration-charge>0) {
										System.out.println(predicationBase-predicationGeneration-charge);
										LimitedBuyingQyt += predicationBase;
										LimitedBuyingQyt -= predicationGeneration;
										predicationBase = 0;
										myAgent.addBehaviour(new LimitedBuyingProcess());
									}else {
										predicationBase = 0;
									}
								}	
								if(temp.equals("close")) {
									System.out.println("retailer agent is offline");
								}		
							}else {
								System.out.println("something error");
							}
						}else {
							block();
						}
					}
				});	
				
				// receive the energy need from application
				// ask battery to provide energy
				// left all other part in Energy Usage process to do
				addBehaviour(new CyclicBehaviour() {
					public void action() {
						MessageTemplate msgTmp = MessageTemplate.MatchConversationId("application-require-energy");
						ACLMessage msg = receive(msgTmp);
						if (msg != null) {
							if (msg.getPerformative() == ACLMessage.CFP) {
								if (msg.getConversationId().equals("application-require-energy")) {
									System.out.println(msg.getSender().getLocalName() + " require: " + msg.getContent() + " units of power");
									float i = Float.parseFloat(msg.getContent());
									energyNeed+= i;
									predicationBase +=i;
									System.out.println("||||||Current predicationBase:" + predicationBase);
									System.out.println("||||||Current energyNeed:" + energyNeed);
								}
							}else {
								block();}			
						}else {
							block();}
					}
			
				});
				
				// ASK battery to provide energy
				addBehaviour(new TickerBehaviour(this, 10000) {
					protected void onTick() {
						if (energyNeed> 0) {
								myAgent.addBehaviour(new EnergyUsageProcess());
								System.out.println("trigger a energy usage sequence");
						}
						else {
							System.out.println("wait for incoming usage info");
						}
					}
				});
	
	}


		// this process use a switch state change to complete the energy consuming 
		// trigger the small amount of buying process
	private class EnergyUsageProcess extends Behaviour {
		private AID bestRetailer;
		private MessageTemplate mt;
		private String state = "SendingRequest";
		private float bestPrice = 10000;
		private float BuyingQty = 0;
		private int responsesCnt = 0;
		@Override
		public void action() {
			BuyingQty += getEnergyNeed();
			setEnergyNeed();
			switch (state) {
			case "SendingRequest":
				//ask battery give energy
				ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);
	
				cfpMsg.addReceiver(new AID("Battery", AID.ISLOCALNAME));			
				cfpMsg.setContent(String.valueOf(BuyingQty)); // how much energy need
				cfpMsg.setConversationId("home-require-energy");
				cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
				myAgent.send(cfpMsg);
				// Prepare the message template to get proposals
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("home-require-energy"),
							MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith())); 
		
				state = "ContinueOrFail";
				System.out.println("Ask battery with the energy: "+ BuyingQty);
				break;
			case "ContinueOrFail":
				ACLMessage response = myAgent.receive(mt);
				if (response != null) {
			
					// Response received
					if(response.getPerformative() == ACLMessage.PROPOSE) {
						float supplyLeak = Float.parseFloat(response.getContent());
						if(supplyLeak > 0) {
							//if the response is higher than 0, that means it exceeds battery output
							//trigger a small amount energy buying
							BuyingQty = supplyLeak;
							System.out.println("Battery has no enough output, need buy extra energy");
							
							state = "StartSmallBuyingProcess";
						}else {
							System.out.println("Battery has enough output, process complete");
							//if the response is less or equal to 0, that means battery output such amount of energy				
							
							state = "done";
						}
					}else {
						block();
					}

				}else {
					block();
				}
				break;
				// needs implementation
			case "StartSmallBuyingProcess":
				System.out.println(getLocalName() + " sent requests to retailer agents");
				// Send the cfp message to all retailers
				cfpMsg = new ACLMessage(ACLMessage.CFP);
				for (AID retailer : retailerList.keySet()) {
					cfpMsg.addReceiver(retailer);
				}
				
				cfpMsg.setConversationId("energy-buying-small-amount");
				cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
				myAgent.send(cfpMsg);
				// Prepare the message template to get proposals
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-buying-small-amount"),
										MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith())); // detect different retailers' reply by unique generated value before with time stamp
				
				state = "FindBestretailer";
				break;
			case "FindBestretailer":
				response = myAgent.receive(mt);
				if (response != null) {
                 
					// Response received
					if(response.getPerformative() == ACLMessage.PROPOSE) {
						// Extract this offer information
						System.out.println(response.getSender().getLocalName() + " offer price: " + response.getContent() + " per kWh");
						float price = Float.parseFloat(response.getContent());
						
                     if(price < bestPrice){
                    	 bestPrice = price;
                    	 bestRetailer = response.getSender();       
                     }
						
					}
					responsesCnt++;
					// Check if receive all responses
					if (responsesCnt == retailerList.size()) {
							state = "SendBuyingConfirm";
							responsesCnt = 0;	
					}

				}
				else {
					block();
				}
				
				break;
			case "SendBuyingConfirm":
				// Send the purchase order to the best retailer
				ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
				order.addReceiver(bestRetailer);
				order.setContent(Float.toString(BuyingQty));
				order.setConversationId("energy-confirm-small-amount");
				order.setReplyWith("order" + System.currentTimeMillis());
				myAgent.send(order);
				// Set a new template to get the purchase order reply
				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-confirm-small-amount"), 
										MessageTemplate.MatchInReplyTo(order.getReplyWith()));
				state = "ReceiveConfirm";
				System.out.println(getLocalName() + " accept the offer of " + bestRetailer.getLocalName());
				break;
			case "ReceiveConfirm":
				response = myAgent.receive(mt);
				if (response != null) {
					// The response message received
					if (response.getPerformative() == ACLMessage.CONFIRM) {
						// Purchased successfully
						System.out.println(response.getContent());
					}
					state = "done";
				}
				else {
					block();
				}
				
				break;
			}
	
	}

		@Override
			public boolean done() {
				return state=="done";
			}
}



	// only be triggered when the battery don't have enough energy as predicted for next day normal usage
	// also with the limited retailer agent is online
	private class LimitedBuyingProcess extends Behaviour {
		private float temp = 0;
		private float price = 0;
		private AID bestRetailer;
		private MessageTemplate mt;
		private String state = "SendingRequest";

	@Override
	public void action() {
		temp = LimitedBuyingQyt;
		/**
		 *only for debuging 
		 *set temp = 1000
		 **/
		// temp = 1000;
		switch (state) {
		case "SendingRequest":
			System.out.println(getLocalName() + " sent requests to limited retailer agents");
			// Send the cfp message to all retailers
			ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);
			for (AID retailer : retailerList.keySet()) {
				cfpMsg.addReceiver(retailer);
			}
			
			cfpMsg.setConversationId("energy-price-limited");
			cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
			myAgent.send(cfpMsg);
			// Prepare the message template to get proposals
			mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-price-limited"),
									MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith())); // detect different retailers' reply by unique generated value before with time stamp
			state = "FindRetailer";
			break;
		case "FindRetailer":
			ACLMessage response = myAgent.receive(mt);
			if (response != null) {
             
				// Response received
				if(response.getPerformative() == ACLMessage.PROPOSE) {
					// Extract this offer information
					System.out.println(response.getSender().getLocalName() + " offer price: " + response.getContent() + " per kWh");
					price = Float.parseFloat(response.getContent());
					bestRetailer = response.getSender(); 
					
					
                 state = "SendTotalPriceQuest";
				}
			}
			else {
				block();
			}
			break;  
		case "SendTotalPriceQuest":
			System.out.println(getLocalName() + " sent requests to retailer agents");
			// Send the cfp message to all retailers
			cfpMsg = new ACLMessage(ACLMessage.CFP);
			cfpMsg.addReceiver(bestRetailer);
			cfpMsg.setContent(String.valueOf(temp));
			cfpMsg.setConversationId("energy-buying-limited");
			cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
			myAgent.send(cfpMsg);
			// Prepare the message template to get proposals
			mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-buying-limited"),
									MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith())); // detect different retailers' reply by unique generated value before with time stamp
			state = "ReceiveTotalPrice";
			break;
		case "ReceiveTotalPrice":
			response = myAgent.receive(mt);
			if (response != null) {    
				// Response received
				if(response.getPerformative() == ACLMessage.PROPOSE) {
					// Extract this offer information
					System.out.println(response.getSender().getLocalName() + " offer a total price: " + response.getContent() );			
                 state = "SendQuestToBuy";
				}
			}
			else {
				block();
			}
			break;
			
		case "SendQuestToBuy":
			// Send the purchase order to the best retailer
			ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
			order.addReceiver(bestRetailer);
			order.setConversationId("energy-confirm-limited");
			order.setReplyWith("order" + System.currentTimeMillis());
			myAgent.send(order);
			// Set a new template to get the purchase order reply
			mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-confirm-limited"), 
									MessageTemplate.MatchInReplyTo(order.getReplyWith()));
			state = "ReceiveConfirm";
			System.out.println(getLocalName() + " accept the offer of " + bestRetailer.getLocalName());
			break;
		case "ReceiveConfirm":
			// Receive the purchase confirmation from the retailer and finish the trade
			response = myAgent.receive(mt);
			if (response != null) {
				// The response message received
				if (response.getPerformative() == ACLMessage.CONFIRM) {
					// Purchased successfully
					System.out.println(response.getContent());
				}
				LimitedBuyingQyt = 0; // reset the buying Quantity
				state = "done";
			}
			else {
				block();
			}
			break;
		}
		
	}

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return state=="done";
	}
	
	
}
	//This class handle the normal contract buying request process
	//Doesn't provide how much energy needs, just ask for a best contract
	private class StartNormalBuyingRequest extends Behaviour {
		private Map<AID, Float> retailerAgentListLocal = new HashMap<AID, Float>();
		private AID bestRetailer;
		// use this two to show on screen
		private float bestPrice = 10000;
		private float buyingQty = 30;
		private int supplyperiod = 0;
		
		private int responsesCnt = 0;
		private MessageTemplate mt;
		private String state = "SendingRequest";
		//change to energyNeed when ayalysis is done
		private int requestCnt = 0;
		private Map<AID, String> AcceptableRetailerList = new HashMap<AID, String>();
		
		public void action() {
			
				switch (state) {
				case "SendingRequest":
					System.out.println(getLocalName() + " sent requests to retailer agents");
					// Send the cfp message to all retailers
					ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);
					for (AID retailer : retailerList.keySet()) {
						cfpMsg.addReceiver(retailer);
					}
					
					cfpMsg.setConversationId("energy-buying");
					cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
					myAgent.send(cfpMsg);
					// Prepare the message template to get proposals
					mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-buying"),
											MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith())); // detect different retailers' reply by unique generated value before with time stamp
					state = "FindRetailer";
					break;
				case "FindRetailer":
					// Receive all the proposals from retailer agents
					// We assume that all retailers are happy to propose their products (No point to refuse customers)
					// Checking and collect all retailer agent that provide enough energy
					ACLMessage response = myAgent.receive(mt);
					if (response != null) {
                     
						// Response received
						if(response.getPerformative() == ACLMessage.PROPOSE) {
							// Extract this offer information
							System.out.println(response.getSender().getLocalName() + " offer power: " + response.getContent() + " kWh");
							float amount = Float.parseFloat(response.getContent());
							//remember the quantity been provided for this time
							retailerAgentListLocal.put(response.getSender(), amount);
                         if(amount > buyingQty){
                        	AcceptableRetailerList.put(response.getSender(), response.getSender().getName());
                             
                         }
							
						}
						//System.out.println("debuging log" + responsesCnt);
						responsesCnt++;
						// Check if receive all responses
						if (responsesCnt == retailerList.size()) {
								state = "SendQuestToFindPrice";
								responsesCnt = 0;	
						}

					}
					else {
						block();
					}
					break;
					// These cases are prepared for negotation process
				case "SendQuestToFindPrice":
					
					// Send the cfp message to all retailers
					cfpMsg = new ACLMessage(ACLMessage.CFP);
					for (AID retailer : AcceptableRetailerList.keySet()) {
						cfpMsg.addReceiver(retailer);
						//System.out.println("sent to "+retailer.getLocalName() +" for price");
					}
					cfpMsg.setContent(String.valueOf(buyingQty));
					cfpMsg.setConversationId("energy-price");
					cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
					myAgent.send(cfpMsg);
					// Prepare the message template to get proposals
					mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-price"),
											MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith())); // detect different retailers' reply by unique generated value before with time stamp
					state = "FindPrice";
					break;
				case "FindPrice":
					response = myAgent.receive(mt);
					if (response != null) {
                     
						// Response received
						if(response.getPerformative() == ACLMessage.PROPOSE) {
							// Extract this offer information
							System.out.println(response.getSender().getLocalName() + " offer price: " + response.getContent() + " per kWh");
							float price = Float.parseFloat(response.getContent());
							
                         if(price < bestPrice){
                        	 bestPrice = price;
                        	 bestRetailer = response.getSender();       
                         }
							
						}
						responsesCnt++;
						// Check if receive all responses
						if (responsesCnt == AcceptableRetailerList.size()) {
							for (AID retailer : retailerAgentListLocal.keySet()) {
								if( retailer.equals(bestRetailer)) {
									buyingQty = retailerAgentListLocal.get(bestRetailer);
								}
								//System.out.println("sent to "+retailer.getLocalName() +" for price");
							}
								state = "AskSupplyPeriod";
								responsesCnt = 0;	
						}

					}
					else {
						block();
					}			
					break;
				case "AskSupplyPeriod":
					cfpMsg = new ACLMessage(ACLMessage.CFP);
					
					cfpMsg.addReceiver(bestRetailer);
					
					cfpMsg.setConversationId("energy-period");
					cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
					myAgent.send(cfpMsg);
					// Prepare the message template to get proposals
					mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-period"),
											MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith())); // detect different retailers' reply by unique generated value before with time stamp
					state = "GetSupplyPeriod";
					// Tell all other retailer agent that them been refused
					cfpMsg = new ACLMessage(ACLMessage.REFUSE);
					for (AID retailer : retailerList.keySet()) {
						if( !retailer.equals(bestRetailer))
						cfpMsg.addReceiver(retailer);
						cfpMsg.setConversationId("refuse");
					}
					myAgent.send(cfpMsg);
					break;
				case "GetSupplyPeriod":	
					response = myAgent.receive(mt);
					if (response != null) {
						// Response received
						if(response.getPerformative() == ACLMessage.PROPOSE) {
							// Extract this offer information
							System.out.println(response.getSender().getLocalName() + " offer period: " + response.getContent());				
							supplyperiod = (int) Float.parseFloat(response.getContent());
						}
						state = "SendOrder";

					}
					else {
						block();
					}	
					
					break;
				case "SendOrder":
					// Send the purchase order to the best retailer
					ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
					order.addReceiver(bestRetailer);
					order.setConversationId("energy-confirm");
					order.setReplyWith("order" + System.currentTimeMillis());
					myAgent.send(order);
					// Set a new template to get the purchase order reply
					mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-confirm"), 
											MessageTemplate.MatchInReplyTo(order.getReplyWith()));
					state = "ReceiveConfirm";
					System.out.println(getLocalName() + " accept the offer of " + bestRetailer.getLocalName());

					break;
				case "ReceiveConfirm":
					// Receive the purchase confirmation from the retailer and finish the trade
					response = myAgent.receive(mt);
					if (response != null) {
						// The response message received
						if (response.getPerformative() == ACLMessage.CONFIRM) {
							// Purchased successfully
							System.out.println(response.getContent());
						}
						state = "done";
					}
					else {
						block();
					}
					break;
				}
		}
		
		@Override
		public boolean done() {
			return state == "done";
		}
	}
	
	//This class handle the whole Selling request process
	//haven't really implement yet
		private class StartSellingRequest extends Behaviour {
			private AID bestRetailer;
			private float bestPrice = 0;
			private int responsesCnt = 0;
			private MessageTemplate mt;
			private String state = "SendingRequest";
			//change to energyNeed when ayalysis is done
			//
			private float SellingQty = (float) (charge*0.2);
			private int requestCnt = 0;
			private Map<AID, String> AcceptableRetailerList = new HashMap<AID, String>();
			
			public void action() {
				
					switch (state) {
					case "SendingRequest":
						System.out.println(getLocalName() + " sent requests to retailer agents");
						// Send the cfp message to all retailers
						ACLMessage cfpMsg = new ACLMessage(ACLMessage.CFP);
						for (AID retailer : retailerList.keySet()) {
							cfpMsg.addReceiver(retailer);
						}
						
						cfpMsg.setConversationId("energy-selling");
						cfpMsg.setReplyWith("cfp" + System.currentTimeMillis()); // Unique value
						myAgent.send(cfpMsg);
						// Prepare the message template to get proposals
						mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-selling"),
												MessageTemplate.MatchInReplyTo(cfpMsg.getReplyWith())); // detect different retailers' reply by unique generated value before with time stamp
						state = "FindRetailer";
						break;
					case "FindRetailer":
						// Receive all the proposals from retailer agents
						// We assume that all retailers are happy to propose their products (No point to refuse customers)
						// Checking and collect all retailer agent that provide enough energy
						ACLMessage response = myAgent.receive(mt);
						if (response != null) {
	                     
							// Response received
							if(response.getPerformative() == ACLMessage.PROPOSE) {
								// Extract this offer information
								System.out.println(response.getSender().getLocalName() + " offer price: " + response.getContent() + " per kWh");
								float price = Float.parseFloat(response.getContent());
								
	                         if(price > bestPrice){
	                        	 bestPrice = price;
	                        	 bestRetailer = response.getSender();
	                         }
								
							}
							responsesCnt++;
							// Check if receive all responses
							if (responsesCnt == retailerList.size()) {
									state = "SendQuestToBuy";
									responsesCnt = 0;	
							}

						}
						else {
							block();
						}
						break;
						// These cases are prepared for negotation process
					case "SendQuestToBuy":
						ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
				
						order.addReceiver(bestRetailer);
						order.setContent(String.valueOf(SellingQty));
						order.setConversationId("energy-selling-confirm");
						order.setReplyWith("order" + System.currentTimeMillis()); // Unique value
						myAgent.send(order);
						// Prepare the message template to get proposals
						mt = MessageTemplate.and(MessageTemplate.MatchConversationId("energy-selling-confirm"),
												MessageTemplate.MatchInReplyTo(order.getReplyWith())); // detect different retailers' reply by unique generated value before with time stamp
						state = "ConfirmBuying";
						System.out.println(getLocalName() + " accept the offer of " + bestRetailer.getLocalName());

						break;
					case "ConfirmBuying":
						response = myAgent.receive(mt);
						if (response != null) {
							if (response.getPerformative() == ACLMessage.CONFIRM) {
							// Purchased successfully
							System.out.println(response.getContent());
							}
						
							state = "done";
						}else {
							block();
						}
	
						break;				
					}
			}
			
			@Override
			public boolean done() {
				return state == "done";
			}
		}
    

	
    
    
}