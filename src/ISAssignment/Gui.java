
// @author Timothy Quill 100997474

package ISAssignment;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultCaret;

public class Gui extends JFrame {
	public static JFrame frame;

	// button labels
	private JButton btnScenario1;
	private JButton btnScenario2;
	private JButton btnQuit;
	private JButton btnRestart;

	// image labels
	private static JLabel lblHeating = new JLabel();
	private static JLabel lblFridge = new JLabel();
	private static JLabel lblLighting = new JLabel();
	private static JLabel lblCar = new JLabel();
	private static JLabel lblFixedPriceRetailer = new JLabel();
	private static JLabel lblInDemandRetailer = new JLabel();
	private static JLabel lblBulkRetailer = new JLabel();
	private static JLabel lblLimitedRetailer = new JLabel();
	private static JLabel lblBattery = new JLabel();
	private static JLabel lblHome = new JLabel();
	private static JLabel lblSolarPanel = new JLabel();

	// text labels
	private static JLabel lblHeatingText = new JLabel();
	private static JLabel lblFridgeText = new JLabel();
	private static JLabel lblLightingText = new JLabel();
	private static JLabel lblCarText = new JLabel();
	private static JLabel lblFixedPriceRetailerText = new JLabel();
	private static JLabel lblInDemandRetailerText = new JLabel();
	private static JLabel lblBulkRetailerText = new JLabel();
	private static JLabel lblLimitedRetailerText = new JLabel();
	private static JLabel lblBatteryText = new JLabel();
	private static JLabel lblTimeDateText = new JLabel();
	private static JLabel lblBoughtText = new JLabel();
	private static JLabel lblSoldText = new JLabel();
	private static JLabel lblUsedText = new JLabel();
	private static JLabel lblPaidText = new JLabel();
	private static JLabel lblSavedText = new JLabel();
	private static JLabel lblSolarPanelText = new JLabel();

	// text
	private static String boughtText;
	private static String soldText;
	private static String usedText;
	private static String paidText;
	private static String savedText;
	private static String heatingText;
	private static String fridgeText;
	private static String lightingText;
	private static String carText;
	private static String fixedPriceRetailerText;
	private static String inDemandRetailerText;
	private static String bulkRetailerText;
	private static String limitedRetailerText;
	private static String batteryText;
	private static String timeDateText;

	private static float bought = 0f;
	private static float sold = 0f;
	private static float used = 0f;
	private static float saved = 0f;
	private static float paid = 0f;

	// heating fields
	private static float heatingCurrentUsage;
	private static float heatingTurnOffTime;
	private static float heatingExpectedUsage;

	// fridge fields
	private static float fridgeCurrentUsage;
	private static float fridgeTurnOffTime;
	private static float fridgeExpectedUsage;

	// lighting fields
	private static float lightingCurrentUsage;
	private static float lightingTurnOffTime;
	private static float lightingExpectedUsage;

	// car fields
	private static float carCurrentUsage;
	private static float carTurnOffTime;
	private static float carExpectedUsage;

	// FixedPriceRetailer fields
	private static float fixedPriceRetailerSellingPrice = 0f;
	private static float fixedPriceRetailerBuyingPrice = 0f;

	// InDemandRetailer fields
	private static float inDemandRetailerSellingPrice;
	private static float inDemandRetailerBuyingPrice;

	// BulkRetailer fields
	private static float bulkRetailerSellingPrice;
	private static float bulkRetailerBuyingPrice;

	// LimitedRetailer fields
	private static float limitedRetailerSellingPrice;
	private static float limitedRetailerBuyingPrice;

	// battery fields
	private static float elecStored = 0f;
	private static float storageLimit = 5f;
	private static JPanel batteryColor = new JPanel();

	// solar panel fields
	private static float solarPanelCurrentCharge;
	
	// set scroll bar fields
	private static JTextArea textArea = new JTextArea();
	private JPanel panel = new JPanel();
	private JScrollPane scrollPane = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

	// start program?
	String scenario = "none";

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		// create window
		frame = new JFrame();
		frame.setBounds(100, 100, 1000, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		/**
		 * handles all of the button functionality
		 */
		btnScenario1 = new JButton("Scenario 1");
		btnScenario1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				scenario = "scenario 1";

			}
		});
		btnScenario1.setBounds(14, 565, 89, 38);
		frame.getContentPane().add(btnScenario1);

		btnScenario2 = new JButton("Scenario 2");
		btnScenario2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				scenario = "scenario 2";
			}
		});
		btnScenario2.setBounds(104, 565, 89, 38);
		frame.getContentPane().add(btnScenario2);

		btnQuit = new JButton("Quit");
		btnQuit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnQuit.setBounds(288, 565, 89, 38);
		frame.getContentPane().add(btnQuit);

		btnRestart = new JButton("Restart");
		btnRestart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateScrollPaneText("hi!");
			}
		});
		btnRestart.setBounds(195, 565, 89, 38);
		frame.getContentPane().add(btnRestart);

		// paints all of the images into the frame
		initialiseImages();

		//
		// place text in the frame:
		//

		// initialise scrool pane
		initialiseScrollPaneText();

		// initialise time and date
		initialiseTimeDate();

		// initialise appliance text
		initialiseHeatingText();
		initialiseFridgeText();
		initialiseLightingText();
		initialiseCarText();

		// set retailer data
		initialiseFixedPriceRetailerText();
		initialiseInDemandRetailerText();
		initialiseBulkRetailerText();
		initialiseLimitedRetailerText();

		// initialise solar panel
		initialiseSolarPanelText();

		// sets up battery
		initialiseBattery();

		// set panel
		initialisePanel();
	}

	public void initialiseImages() {
		// creates Home image
		lblHome = new JLabel("");
		Image home = new ImageIcon(this.getClass().getResource("/home.png")).getImage();
		lblHome.setIcon(new ImageIcon(home));
		lblHome.setBounds(423, 128, 150, 157);
		frame.getContentPane().add(lblHome);

		// sets the heating image
		lblHeating = new JLabel("");
		Image heating = new ImageIcon(this.getClass().getResource("/heating.png")).getImage();
		lblHeating.setIcon(new ImageIcon(heating));
		lblHeating.setBounds(22, 18, 97, 88);
		frame.getContentPane().add(lblHeating);

		// sets the fridge image
		lblFridge = new JLabel("");
		Image fridge = new ImageIcon(this.getClass().getResource("/fridge.png")).getImage();
		lblFridge.setIcon(new ImageIcon(fridge));
		lblFridge.setBounds(22, 134, 97, 93);
		frame.getContentPane().add(lblFridge);

		// sets the lighting image
		lblLighting = new JLabel("");
		Image lighting = new ImageIcon(this.getClass().getResource("/lighting.png")).getImage();
		lblLighting.setIcon(new ImageIcon(lighting));
		lblLighting.setBounds(22, 247, 97, 101);
		frame.getContentPane().add(lblLighting);

		// sets the car image
		lblCar = new JLabel("");
		Image car = new ImageIcon(this.getClass().getResource("/car.png")).getImage();
		lblCar.setIcon(new ImageIcon(car));
		lblCar.setBounds(22, 381, 97, 48);
		frame.getContentPane().add(lblCar);

		// sets the FixedPriceRetailer image
		lblFixedPriceRetailer = new JLabel("");
		Image fixedPriceRetailer = new ImageIcon(this.getClass().getResource("/fixed_price_retailer.png")).getImage();
		lblFixedPriceRetailer.setIcon(new ImageIcon(fixedPriceRetailer));
		lblFixedPriceRetailer.setBounds(885, 13, 97, 93);
		frame.getContentPane().add(lblFixedPriceRetailer);

		// sets the InDemandRetailer image
		lblInDemandRetailer = new JLabel("");
		Image inDemandRetailer = new ImageIcon(this.getClass().getResource("/in_demand_retailer.png")).getImage();
		lblInDemandRetailer.setIcon(new ImageIcon(inDemandRetailer));
		lblInDemandRetailer.setBounds(885, 118, 97, 93);
		frame.getContentPane().add(lblInDemandRetailer);

		// sets the BulkRetailer image
		lblBulkRetailer = new JLabel("");
		Image bulkRetailer = new ImageIcon(this.getClass().getResource("/bulk_retailer.png")).getImage();
		lblBulkRetailer.setIcon(new ImageIcon(bulkRetailer));
		lblBulkRetailer.setBounds(885, 227, 97, 93);
		frame.getContentPane().add(lblBulkRetailer);

		// sets the LimitedRetailer image
		lblLimitedRetailer = new JLabel("");
		Image limitedRetailer = new ImageIcon(this.getClass().getResource("/limited_retailer.png")).getImage();
		lblLimitedRetailer.setIcon(new ImageIcon(limitedRetailer));
		lblLimitedRetailer.setBounds(885, 330, 97, 93);
		frame.getContentPane().add(lblLimitedRetailer);

		// sets the battery image
		lblBattery = new JLabel("");
		Image battery = new ImageIcon(this.getClass().getResource("/battery.png")).getImage();
		lblBattery.setIcon(new ImageIcon(battery));
		lblBattery.setBounds(22, 467, 97, 68);
		frame.getContentPane().add(lblBattery);

		// sets the solar panel image
		lblSolarPanel = new JLabel("");
		Image solar_panel = new ImageIcon(this.getClass().getResource("/solar_panel.png")).getImage();
		lblSolarPanel.setIcon(new ImageIcon(solar_panel));
		lblSolarPanel.setBounds(361, 53, 97, 66);
		frame.getContentPane().add(lblSolarPanel);
	}

	/**
	 * handles all of the scrollbar text
	 */

	public void initialiseScrollPaneText() {
		updateScrollPaneText("");
		textArea.setForeground(Color.RED);
		panel.add(textArea);
		panel.setBackground(Color.WHITE);
		scrollPane.setBounds(312, 322, 334, 185);

		frame.getContentPane().add(scrollPane);
	}

	public static void updateScrollPaneText(String _scrollPaneText) {
		
		if (_scrollPaneText != "")
		{
			_scrollPaneText = timeDateText + " - " + _scrollPaneText + "\n";
		}

		textArea.append(_scrollPaneText);
		DefaultCaret caret = (DefaultCaret)textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		textArea.repaint();
	}

	/**
	 * handles all of the right-bottom panel presentation
	 */
	public void initialisePanel() {
		// set bought electricity text
		setElecBought(bought);
		lblBoughtText.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblBoughtText.setForeground(Color.BLACK);
		lblBoughtText.setBounds(710, 455, 261, 25);
		frame.getContentPane().add(lblBoughtText);

		// set sold electricity text
		setElecSold(sold);
		lblSoldText.setForeground(Color.BLACK);
		lblSoldText.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblSoldText.setBounds(710, 482, 261, 25);
		frame.getContentPane().add(lblSoldText);

		// set used electricity text
		setElecUsed(used);
		lblUsedText.setForeground(Color.BLACK);
		lblUsedText.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblUsedText.setBounds(710, 507, 261, 27);
		frame.getContentPane().add(lblUsedText);

		// set paid $ text
		setElecPaid(used);
		lblPaidText.setForeground(Color.BLACK);
		lblPaidText.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblPaidText.setBounds(713, 535, 258, 25);
		frame.getContentPane().add(lblPaidText);

		// set saved $ text
		setElecSaved();
		lblSavedText.setForeground(Color.BLACK);
		lblSavedText.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		lblSavedText.setBounds(711, 561, 257, 25);
		frame.getContentPane().add(lblSavedText);
	}

	/**
	 * handles all of the date presentation
	 */
	public void initialiseTimeDate() {
		// sets the date text
		lblTimeDateText.setForeground(Color.BLACK);
		lblTimeDateText.setBounds(437, 570, 214, 16);
		frame.getContentPane().add(lblTimeDateText);
	}

	/**
	 * handles all of the solar panel data presentation
	 */
	public void initialiseSolarPanelText() {
		setSolarPanelText(solarPanelCurrentCharge);
		lblSolarPanelText.setBounds(472, 72, 131, 34);
		frame.getContentPane().add(lblSolarPanelText);

		lblSolarPanelText.setForeground(Color.RED);
		lblSolarPanelText.setVerticalAlignment(SwingConstants.TOP);
	}

	/**
	 * handles all of the heating data presentation
	 */
	public void initialiseHeatingText() {
		setHeatingText(heatingCurrentUsage);
		lblHeatingText.setBounds(131, 34, 181, 51);
		frame.getContentPane().add(lblHeatingText);

		lblHeatingText.setForeground(Color.RED);
		lblHeatingText.setVerticalAlignment(SwingConstants.TOP);
	}

	/**
	 * handles all of the fridge data presentation
	 */
	public void initialiseFridgeText() {
		setFridgeText(fridgeCurrentUsage);
		lblFridgeText.setBounds(131, 154, 181, 51);
		frame.getContentPane().add(lblFridgeText);
		lblFridgeText.setForeground(Color.RED);
		lblFridgeText.setVerticalAlignment(SwingConstants.TOP);
	}

	/**
	 * handles all of the lighting data presentation
	 */
	public void initialiseLightingText() {
		setLightingText(lightingCurrentUsage);
		lblLightingText.setBounds(131, 263, 181, 51);
		frame.getContentPane().add(lblLightingText);
		lblLightingText.setForeground(Color.RED);
		lblLightingText.setVerticalAlignment(SwingConstants.TOP);
	}

	/**
	 * handles all of the lighting data presentation
	 */
	public void initialiseCarText() {
		setCarText(carCurrentUsage);
		lblCarText.setBounds(131, 378, 181, 51);
		frame.getContentPane().add(lblCarText);
		lblCarText.setForeground(Color.RED);
		lblCarText.setVerticalAlignment(SwingConstants.TOP);
	}

	/**
	 * handles all of the lblFixedPriceRetailer data presentation
	 */
	public void initialiseFixedPriceRetailerText() {
		// sets the FixedPriceRetailer text
		setFixedPriceRetailerText(fixedPriceRetailerSellingPrice, "buying");
		lblFixedPriceRetailerText.setBounds(692, 34, 181, 51);
		frame.getContentPane().add(lblFixedPriceRetailerText);
		lblFixedPriceRetailerText.setForeground(Color.RED);
		lblFixedPriceRetailerText.setVerticalAlignment(SwingConstants.TOP);
	}

	/**
	 * handles all of the FixedPriceRetailer data presentation
	 */
	public void initialiseInDemandRetailerText() {
		setInDemandRetailerText(inDemandRetailerBuyingPrice, "buying");
		lblInDemandRetailerText.setBounds(692, 136, 181, 51);
		frame.getContentPane().add(lblInDemandRetailerText);
		lblInDemandRetailerText.setForeground(Color.RED);
		lblInDemandRetailerText.setVerticalAlignment(SwingConstants.TOP);
	}

	public void initialiseBulkRetailerText() {
		setBulkRetailerText(bulkRetailerSellingPrice, "buying");
		lblBulkRetailerText.setBounds(692, 249, 181, 51);
		frame.getContentPane().add(lblBulkRetailerText);
		lblBulkRetailerText.setForeground(Color.RED);
		lblBulkRetailerText.setVerticalAlignment(SwingConstants.TOP);
	}

	public void initialiseLimitedRetailerText() {
		setLimitedRetailerText(limitedRetailerSellingPrice, "buying", false);
		lblLimitedRetailerText.setBounds(692, 345, 181, 51);
		frame.getContentPane().add(lblLimitedRetailerText);
		lblLimitedRetailerText.setForeground(Color.RED);
		lblLimitedRetailerText.setVerticalAlignment(SwingConstants.TOP);
	}

	public void initialiseBattery() {
		// sets the battery text
		setBatteryText(0f, 0f);
		lblBatteryText.setBounds(131, 479, 181, 38);
		frame.getContentPane().add(lblBatteryText);
		lblBatteryText.setForeground(Color.RED);
		lblBatteryText.setVerticalAlignment(SwingConstants.TOP);

		// animates the battery's color
		animateBattery();
	}

	public static void animateBattery() {
		float boxPercent = 58.0f / 100.0f;
		float batteryPercent = 100f / storageLimit * elecStored;
		setBatteryColor(batteryPercent);

		float storedPercent = boxPercent * batteryPercent;
		int a = Math.round(storedPercent);
		batteryColor.setBounds(27, 535 - a, 81, a); // 22, 467, 97, 68
		frame.getContentPane().add(batteryColor);
	}

	public static void setBatteryColor(float stored) {
		if (stored < 25f) {
			batteryColor.setBackground(Color.RED);
		} else if (stored < 75f) {
			batteryColor.setBackground(Color.YELLOW);
		} else {
			batteryColor.setBackground(Color.GREEN);
		}
	}

	public void setTimeDate(String _timeDate) {
		timeDateText = _timeDate;
		lblTimeDateText.setText(timeDateText);
		lblTimeDateText.repaint();
	}

	public static void setSolarPanelText(float _solarPanelCurrentUsage) {
		solarPanelCurrentCharge = _solarPanelCurrentUsage;
		lblSolarPanelText.setText("Charge: " + solarPanelCurrentCharge + " kWs");
		lblSolarPanelText.repaint();
	}

	public static void setBatteryText(float _elecStored, float _storageLimit) {
		elecStored = _elecStored;
		storageLimit = _storageLimit;
		batteryText = "<html>" + Math.round(elecStored * 100.0) / 100.00 + " kWs stored of<br>a "
				+ Math.round(storageLimit * 100.0) / 100.00 + " kW capacity";

		lblBatteryText.setText(batteryText);
		lblBatteryText.repaint();

		animateBattery();
		batteryColor.repaint();
		lblBattery.setVisible(false);
		lblBattery.setVisible(true);
	}

	public static void setElecUsed(float _used) {
		used = _used;
		usedText = "   Used (kWs): " + Math.round(used * 100.0) / 100.00;

		lblUsedText.setText(usedText);
		lblUsedText.repaint();
		setElecSaved();
	}

	public static void setElecBought(float _bought) {
		bought = _bought;
		boughtText = "Bought (kWs): " + Math.round(bought * 100.0) / 100.00;

		lblBoughtText.setText(boughtText);
		lblBoughtText.repaint();
	}

	public static void setElecPaid(float _paid) {
		paid = _paid;
		paidText = "        Paid ($): " + Math.round(paid * 100.0) / 100.00;
		lblPaidText.setText(paidText);
		lblPaidText.repaint();
	}

	public static void setElecSold(float _sold) {
		sold = _sold;
		soldText = "    Sold (kWs): " + Math.round(sold * 100.0) / 100.00;

		lblSoldText.setText(soldText);
		lblSoldText.repaint();
	}

	public static void setElecSaved() {
		// (used * fixed price) - bought
		saved = used * fixedPriceRetailerSellingPrice - paid;
		savedText = "      Saved ($): " + Math.round(saved * 100.0) / 100.00;

		lblSavedText.setText(savedText);
		lblSavedText.repaint();
	}

	public static void setCarText(float _carCurrentUsage) {
		carText = "<html>   Current usage: " + carCurrentUsage + "<br>Exp. turn off time: " + carTurnOffTime
				+ "<br>        Exp. usage: " + carExpectedUsage;
		lblCarText.setText(carText);
		lblCarText.repaint();
	}

	public static void setHeatingText(float _heatingCurrentUsage) {
		heatingText = "<html>   Current usage: " + heatingCurrentUsage + "<br>Exp. turn off time: " + heatingTurnOffTime
				+ "<br>        Exp. usage: " + heatingExpectedUsage;

		lblHeatingText.setText(heatingText);
		lblHeatingText.repaint();
	}

	public static void setLightingText(float _lightingCurrentUsage) {
		// sets lighting text
		lightingText = "<html>   Current usage: " + lightingCurrentUsage + "<br>Exp. turn off time: "
				+ lightingTurnOffTime + "<br>        Exp. usage: " + lightingExpectedUsage;

		lblLightingText.setText(lightingText);
		lblLightingText.repaint();
	}

	public static void setFridgeText(float _fridgeCurrentUsage) {
		// sets fridge text
		fridgeText = "<html>   Current usage: " + fridgeCurrentUsage + "<br>Exp. turn off time: " + fridgeTurnOffTime
				+ "<br>        Exp. usage: " + fridgeExpectedUsage;

		lblFridgeText.setText(fridgeText);
		lblFridgeText.repaint();
	}

	public static void setFixedPriceRetailerText(float _fixedPriceRetailerPrice, String buyingSelling) {
		if (buyingSelling == "buying") {
			fixedPriceRetailerBuyingPrice = _fixedPriceRetailerPrice;
		}
		if (buyingSelling == "selling") {
			fixedPriceRetailerSellingPrice = _fixedPriceRetailerPrice;
		}

		fixedPriceRetailerText = new String("<html>Selling at $" + fixedPriceRetailerSellingPrice + " / kWs"
				+ "<br>Buying at $" + fixedPriceRetailerBuyingPrice + " / kWs");
		lblFixedPriceRetailerText.setText(fixedPriceRetailerText);
		lblFixedPriceRetailerText.repaint();
	}

	public static void setBulkRetailerText(float _BulkRetailerPrice, String buyingSelling) {
		if (buyingSelling == "buying") {
			bulkRetailerBuyingPrice = _BulkRetailerPrice;
		}
		if (buyingSelling == "selling") {
			bulkRetailerSellingPrice = _BulkRetailerPrice;
		}

		bulkRetailerText = new String("<html>Selling at $" + bulkRetailerSellingPrice + " / kWs" + "<br>Buying at $"
				+ bulkRetailerBuyingPrice + " / kWs");
		lblBulkRetailerText.setText(bulkRetailerText);
		lblBulkRetailerText.repaint();
	}

	public static void setInDemandRetailerText(float _InDemandPrice, String buyingSelling) {
		if (buyingSelling == "buying") {
			inDemandRetailerBuyingPrice = _InDemandPrice;
		}
		if (buyingSelling == "selling") {
			inDemandRetailerSellingPrice = _InDemandPrice;
		}

		inDemandRetailerText = new String("<html>Selling at $" + inDemandRetailerSellingPrice + " / kWs"
				+ "<br>Buying at $" + inDemandRetailerBuyingPrice + " / kWs");
		lblInDemandRetailerText.setText(inDemandRetailerText);
		lblInDemandRetailerText.repaint();
	}

	public static void setLimitedRetailerText(float _LimitedRetailerPrice, String buyingSelling,
			boolean _limitedIsOpen) {
		if (buyingSelling == "buying") {
			limitedRetailerBuyingPrice = _LimitedRetailerPrice;
		}
		if (buyingSelling == "selling") {
			limitedRetailerSellingPrice = _LimitedRetailerPrice;
		}

		if (_limitedIsOpen) {
			limitedRetailerText = "<html>Selling at $" + limitedRetailerSellingPrice + " / kWs" + "<br>Buying at $"
					+ limitedRetailerBuyingPrice + " / kWs";
		} else {
			limitedRetailerText = "<html>Retailer is closed";
		}

		lblLimitedRetailerText.setText(limitedRetailerText);
		lblLimitedRetailerText.repaint();
	}
}
