#import plaidml.keras
#plaidml.keras.install_backend()
#auther: 100858830 qiyuanzhu
from keras.layers import *
from keras.callbacks import ReduceLROnPlateau, CSVLogger, EarlyStopping, ModelCheckpoint
from keras.models import Model
from keras.preprocessing.text import hashing_trick
from keras import optimizers
from sklearn.model_selection import train_test_split
from keras.regularizers import *
import numpy as np
import pandas as pd
import os.path

lr_reducer = ReduceLROnPlateau(factor=np.sqrt(0.1), cooldown=2, patience=5e5, min_lr=0.5e-6)
early_stopper = EarlyStopping(min_delta=0.0001, patience=1e10,mode='min')
checkpoint = ModelCheckpoint("model.hdf5",save_best_only=True)
csv_logger = CSVLogger('testing_data.csv')

#settings
batch_size=256
nb_epoch = 200


max_value = 2000+1




data = pd.read_csv('training_data.csv',header=0,)





def preprocess(data):
    return [data['conds'].values, data['temp'].values, data['humidity'].values, data['pressure'].values, data['windchill'].values, data['fog'].values, data['rain'].values, data['hail'].values] , data['KW'].values

            
            
def hash_and_pad(str,size):
    x = []
    for i in str:
        if type(i) is float: x.append([])
        else:x.append(hashing_trick(i,size))
        
    return np.array([np.array(xi+[0]*(size-len(xi))) for xi in x])
            
            
"""def preprocess(data):
    #fields = ['Keyword']
    header = data.columns.values.tolist()
    x = []
    for i in data['Keyword']:
        x.append(hashing_trick(i,max_keyword))
    return np.array([np.array(xi+[0]*(max_keyword-len(xi))) for xi in x]) , data['Mark']
"""
X_train, Y_train = preprocess(data)




#data = pd.read_csv('testrecommendation.csv',header=0,dtype=np.float32,usecols=list(range(1,9)))
#X_test, Y_test = preprocess(data)
#X_train, X_test, Y_train, Y_test = train_test_split(X_train, Y_train, test_size=0.1)#,random_state = 42)
#X_train, X_test, Y_train, Y_test

def make_model(shape=(8,)):
    all_shape = (1,)
    #inorder to avoid parser confuse tuple
    
    x = Input(shape=all_shape,dtype='int32')
    #layer = embedding_layer(kw_set)(x)
    X=Embedding(max_value,1,mask_zero=False)(x)#,embeddings_regularizer=l1(0.01)
    X= Flatten()(X)
    
    x1 = Input(shape=all_shape,dtype='int32')
    #layer = embedding_layer(kw_set)(x)
    X1=Embedding(max_value,1,mask_zero=False)(x1)#,embeddings_regularizer=l1(0.01)
    X1= Flatten()(X1)
    
    x2 = Input(shape=all_shape,dtype='int32')
    #layer = embedding_layer(kw_set)(x)
    X2=Embedding(max_value,1,mask_zero=False)(x2)#,embeddings_regularizer=l1(0.01)
    X2= Flatten()(X2)
    
    x3 = Input(shape=all_shape,dtype='int32')
    #layer = embedding_layer(kw_set)(x)
    X3=Embedding(max_value,1,mask_zero=False)(x3)#,embeddings_regularizer=l1(0.01)
    X3= Flatten()(X3)
    
    x4 = Input(shape=all_shape,dtype='int32')
    #layer = embedding_layer(kw_set)(x)
    X4=Embedding(max_value,1,mask_zero=False)(x4)#,embeddings_regularizer=l1(0.01)
    X4= Flatten()(X4)
    
    x5 = Input(shape=all_shape,dtype='int32')
    #layer = embedding_layer(kw_set)(x)
    X5=Embedding(max_value,1,mask_zero=False)(x5)#,embeddings_regularizer=l1(0.01)
    X5= Flatten()(X5)
    
    x6 = Input(shape=all_shape,dtype='int32')
    #layer = embedding_layer(kw_set)(x)
    X6=Embedding(max_value,1,mask_zero=False)(x6)#,embeddings_regularizer=l1(0.01)
    X6= Flatten()(X6)
    
    x7 = Input(shape=all_shape,dtype='int32')
    #layer = embedding_layer(kw_set)(x)
    X7=Embedding(max_value,1,mask_zero=False)(x7)#,embeddings_regularizer=l1(0.01)
    X7= Flatten()(X7)
    #
    #layer =embedding_layer(kw_set)(x)
    #layer = Conv1D(128,32,padding='valid',
    #             activation='relu')(layer)
    layer = Concatenate()([X,X1,X2,X3,X4,X5,X6,X7])
    #layer = Concatenate(axis=-1)(layer,X3)
    #layer = MaxPooling1D(5)(x)
    #layer = Flatten()(x)
    #e = layer
    for i in [100,50,25,10,3]:
        layer = Dense(units=(i),activation = 'relu')(layer)
        layer = BatchNormalization()(layer)
        #layer = concatenate([e,layer])
        #layer = Dropout(0.01)(layer)
    layer = Dense(units=(5),activation = 'relu')(layer)
    layer = Dense(units=(5),activation = 'sigmoid',use_bias=True,kernel_regularizer=l2(0.01))(layer)
    #layer = Flatten()(layer)
    #y = Dense(units=(1))(layer)
    #layer = concatenate([Flatten()(e),layer])
    #layer = Dropout(0.01)(layer)
    y = Dense(units=(1),activation = 'relu')(layer)
    #y = Dropout(0.01)(y)
    #y = Flatten()(y)
    #y=MaxPooling1D(1)(y)
    #y=layer
    model = Model(inputs =[x,x1,x2,x3,x4,x5,x6,x7],outputs=[y])
    return model
    

model = make_model()#X_train.shape[1:])
#sgd = optimizers.Adagrad(lr=0.05, epsilon=0.5e-6, decay=0.0)
model.compile(loss='mean_absolute_error',#'sparse_categorical_crossentropy',#,#'mean_squared_error',
              optimizer='sgd',
              metrics=['mean_squared_error','mean_absolute_error'])

if __name__ == "__main__":
    try:
        model.load_weights("model.hdf5")
    except:
        print("cannot load weight")
    try:
        #1/0
            model.fit(X_train, Y_train,
                      #batch_size=batch_size,
                      epochs=nb_epoch,
                      #steps_per_epoch=20000,
                      #validation_data=(X_test, Y_test),
                      validation_split=0.2,
                      shuffle=True,
                      callbacks=[lr_reducer, checkpoint, early_stopper, csv_logger])
    except Exception as e:
        raise e
        print(e)
    data = pd.read_csv('needpredicationweather.csv',header=0)
    X_test,_= preprocess(data)
    output = model.predict(X_test)
    file = open('testfile.txt','w')
    for i in output:
        temp = str(i)
        print(temp)
        file.writelines(temp)
        file.write(',')
    file.close() 
    print(output)
